
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');



/**
 * Book Package or Service
 *
 */
$('#btn-book').on('click', function() {
    var a = $('#datetimepicker6').data('DateTimePicker').date();
    var b = $('#datetimepicker7').data('DateTimePicker').date();
    var book = {
        book_id : $('#book-id').val(),
        type : $('#type').val(),
        date_reserved:moment(a).format('YYYY-MM-DD'),
        time_reserved: moment(b).format('HH:mm') + ':00',
        branch_id: $('#branch').val(),
        amount: $('#book-amount').val(),
        item: $('#book-item-details').val(),
        reserve_date: moment(a).format('LL') + ' ' + moment(b).format('LT')
    };

    if (a == null || b == null) {
        alert('Date and time are required to book a service or package.');
    } else {
        var selectedDate =  new Date($('#datetimepicker7').data('DateTimePicker').date());
        var dateselected = new Date($('#datetimepicker6').data('DateTimePicker').date());
        var today = new Date();
        var timeYeah = selectedDate.toLocaleTimeString('en-US', { hour12: false });
        var currentTempTime = new Date();
        var currentTime = currentTempTime.toLocaleTimeString('en-US', { hour12: false });
        if (moment(timeYeah, 'HH:mm:ss') >= moment('09:30:00', 'HH:mm:ss') && moment(timeYeah, 'HH:mm:ss') < moment('19:00:00', 'HH:mm:ss')) {

            if (dateselected.setHours(0,0,0,0) == today.setHours(0,0,0,0) && moment(currentTime, 'HH:mm:ss') > moment(timeYeah, 'HH:mm:ss')) {
                alert('Our operation hours is from 9:30 AM to 7:00PM only.');
            } else {
                axios({
                  method: 'POST',
                  url: `/api/reservation/check`,
                  data: book
                }).then(function (response) {
                    if (response.data == true) {
                        localStorage.setItem('reserve', JSON.stringify(book));
                        location.href = '/reserve';
                    } else {
                        alert('Branch, time and date are already booked.');
                    }
                });
            }
        } else {
            alert('Our operation hours is from 9:30 AM to 7:00PM only.');
        }
    }

});


$('#prev').click(function() {
  $('#calendar').fullCalendar('prev');
});

$('#next').click(function() {
  $('#calendar').fullCalendar('next');
});


$('#btn-add-cart').on('click', function() {
    var product = $(this).data('product');
    var count = parseInt(localStorage.getItem('count'));
    if (parseInt($('#quantity').val()) > 0 && product.inventory >= parseInt($('#quantity').val()) ) {
        processMyCart(product, count);
    }
});

function processMyCart(product, count) {
  var addedProduct = JSON.parse(localStorage.getItem('product'));
  var exist = 0;
  var x = document.getElementById("snackbar");
  x.className = "show";

    if (count == 0) {
      localStorage.setItem('count', 0);
    }

    if (addedProduct) {
        $.each(addedProduct, function(key, value) {
            if (value.PROD_ID == product.PROD_ID) {
                if (parseInt($('#quantity').val()) >= product.inventory) {
                    value.quantity = product.inventory;
                    exist = 1;
                } else {
                    value.quantity = parseInt($('#quantity').val());
                    exist = 1;
                }
            }
        });

        if (exist == 0) {
            product['quantity'] = parseInt($('#quantity').val());
            if (parseInt($('#quantity').val()) > product.inventory) {
                product['quantity'] = product.inventory;
            }
            addedProduct.push(product);
            count = count + 1 ;
            localStorage.setItem('count', count);
            $('#badge-cart').text(count);
            $('#order-item').text(count);
        }

        if (exist <= 1) {
            $('#snackbar').text(product['PROD_NAME'] + ' is added to cart.')
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            localStorage.setItem('product', JSON.stringify(addedProduct));
        } else {
            $('#snackbar').text(product['PROD_NAME'] + 'is out of stock.');
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        }
    } else {
        product['quantity'] = parseInt($('#quantity').val());
        if (parseInt($('#quantity').val()) > product.inventory) {
            product['quantity'] = product.inventory;
        }
        var addedProduct = [];
        addedProduct.push(product);
        localStorage.setItem('product',  JSON.stringify(addedProduct));
        count = count + 1 ;
        localStorage.setItem('count', count);
        $('#badge-cart').text(count);
        $('#order-item').text(count);
        $('#snackbar').text(product['PROD_NAME'] + ' is added to cart.')
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
}

$('#quantity').on('keyup', function() {
    var inventory = parseInt($(this).data('inventory'));
    if ($(this).val() > inventory) {
        $(this).val(inventory);
    }
});

$('#btn-checkout').on('click', function() {
    var cartProducts = JSON.parse(localStorage.getItem('product'));
    if (cartProducts) {
        location.href = '/checkout';
    }
});



$(document).ready(function() {
    var cart_count = localStorage.getItem('count');

    is_for_reservation();

    if (cart_count) {
      $('#badge-cart').text(cart_count);
      $('#order-item').text(cart_count);
    } else {
      localStorage.setItem('count', 0);
    }
    var attempts = localStorage.getItem('attempts');

    if (attempts >= 3) {
        $('#login-button').attr('disabled', 'disabled');
        $('#login-button').attr('class', 'btn btn-primary form-control btn-default is-caps disabled');
        var timerStore = localStorage.getItem('timer');

        if (timerStore == '' || timerStore == null) {
            localStorage.setItem('timer', 0);
            timerStore = 0;
        } else {
            timerStore = parseInt(localStorage.getItem('timer'));
        }

        if (timerStore == 0) {
            timerStore = 30;
        }

        console.log(timerStore);
        var timeLeft = timerStore;
        var timerId = setInterval(countdown, 1000);

        function countdown() {
          if (timeLeft == 0) {
            localStorage.setItem('timer', 0);
            clearTimeout(timerId);
            $('#login-button').removeAttr('disabled');
        $('#login-button').attr('class', 'btn btn-primary form-control btn-default is-caps');
            $('.login-error').hide();
            localStorage.setItem('attempts', 0);
            localStorage.setItem('email', '');
          } else {
            localStorage.setItem('timer', timeLeft);
            $('#login-button').attr('disabled', 'disabled');
        $('#login-button').attr('class', 'btn btn-primary form-control btn-default is-caps disabled');
            $('.login-error').html('<div class="alert alert-danger" id="login-error" role="alert"> <span>Too many login attempts. Please try again in ' + timeLeft + ' seconds</span> </div>');
            timeLeft--;
            localStorage.setItem('timer', timeLeft);
          }
        }
    }

    if (cart_count == 0) {
      $('#btn-checkout').attr('disabled', 'disabled');
      $('.btn-login-checkout').attr('disabled', 'disabled');
      $('.btn-login-checkout').attr('href', '');
      $('#proceed-button').attr('disabled', 'disabled');
    }

    $('#biller_province').on('change', function() {
        axios({
          method: 'GET',
          url: `/api/cities?province=${$('#biller_province :selected').val()}`,
        })
        .then(function (response) {
            if (response.data.length > 0) {
                var sHtml = '<option value="" disabled="disabled" selected="selected">Choose City</option>';
                $.each(response.data, function(key, city) {
                    sHtml = sHtml + '<option value="' + city.name + '">' + city.name + '</option>';
                });
                $('#biller_city_select').html(sHtml);
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    $('#shipper_province').on('change', function() {
        axios({
          method: 'GET',
          url: `/api/cities?province=${$('#shipper_province :selected').val()}`,
        })
        .then(function (response) {
            if (response.data.length > 0) {
                var sHtml = '<option value="" disabled="disabled" selected="selected">Choose City</option>';
                $.each(response.data, function(key, city) {
                    sHtml = sHtml + '<option value="' + city.name + '">' + city.name + '</option>';
                });
                $('#shipper_city_select').html(sHtml);
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    $('#pet_category').on('change', function() {
        is_for_reservation();
        axios({
          method: 'GET',
          url: `/api/breeds/${$('#pet_category :selected').val()}`,
        })
        .then(function (response) {
            sHtml = '';
            if (response.data.length > 0) {
                $.each(response.data, function(key, breed) {
                    sHtml = sHtml + '<option value="' + breed.B_ID + '">' + breed.B_NAME + '</option>';
                });
                $('#breed').html(sHtml);
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    });

    $('#breed').on('change', function() {
        is_for_reservation();
    });

    $('#pet_gender').on('change', function() {
        is_for_reservation();
    });

    $('#biller_city_select').on('change', function() {
        selectedCity = $('#biller_city_select :selected').val();
        $('#biller_city').val(selectedCity);
        console.log($('#biller_city').val());
    });

    $('#shipper_city_select').on('change', function() {
        selectedCity = $('#shipper_city_select :selected').val();
        $('#shipper_city').val(selectedCity);
        console.log($('#shipper_city').val());
    });

    function is_for_reservation(){

        if ($('#pet_name').val() == '' || $('#pet_category :selected').val() == '' || $('#breed :selected').val() == '' || $('#pet_gender :selected').val() == '' || $('#pet_color').val() == '' || $('#pet_age').val() == '') {
            $('#paypal-reserve').attr('disabled', 'disabled');
            $('.stripe-button-el').attr('disabled', 'disabled');
            console.log('disable');
        } else {
            $('#paypal-reserve').removeAttr('disabled');
            $('.stripe-button-el').removeAttr('disabled');
            console.log('good');
        }

        var pet_info = JSON.stringify({
            'name' : $('#pet_name').val(),
            'markingcolor' : $('#pet_color').val(),
            'gender' : $('#pet_gender').val(),
            'pet_category_id': $('#pet_category :selected').val(),
            'breed' : $('#breed :selected').val(),
            'age' : $('#pet_age').val()
        });

        localStorage.setItem('pet_information', pet_info);
        $('#pet_information').val(localStorage.getItem('pet_information'));
    }

    $('#pet_name').on('keyup keypress', function(e) {
        is_for_reservation();
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });

    $('#pet_age').on('keyup keypress', function(e) {
        is_for_reservation();
        var regex = new RegExp("^[0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });

    $('#pet_color').on('keyup keypress', function(e) {
        is_for_reservation();
        var regex = new RegExp("^[a-zA-Z ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });

    $(document).on('click', '.product-close', function() {
        var saveProduct = JSON.parse(localStorage.getItem('product'));
        var cart_count = localStorage.getItem('count');
        var total_count = 0;
        var diffSubtotal = 0;
        total_count = cart_count - 1;
        var tempSubTotal = $('#subtotal').text();
        localStorage.setItem('count', total_count);
        var arrayToRemove = $(this).data('index');
        minusPrice = saveProduct[arrayToRemove]['PROD_PRICE'];
        diffSubtotal = tempSubTotal - minusPrice;

        if (diffSubtotal < 0) {
            diffSubtotal = 0;
        }

        if (saveProduct) {
            saveProduct.splice(arrayToRemove, 1);
            $('#subtotal').text(diffSubtotal);
            localStorage.setItem('subtotal', diffSubtotal);
            localStorage.setItem('product', JSON.stringify(saveProduct));
        }

        if (total_count == 0) {
          $('#btn-checkout').attr('disabled', 'disabled');
          $('.btn-login-checkout').attr('disabled', 'disabled');
          $('.btn-login-checkout').attr('href', '');
        }

        location.reload(true);
    });

    var cartProducts = JSON.parse(localStorage.getItem('product'));
    var total = 0;
    if (cartProducts) {
        $.each(cartProducts, function(key, value) {
            total = total + (parseFloat(value.PROD_PRICE).toFixed(2) * value.quantity);
            var sHtml = `<div class="alert alert-default alert-dismissible alert-cart"> <a href="#" data-index="${key}" class="close product-close" data-dismiss="alert" aria-label="close">&times;</a> <div class="cart-content"> <div class="image"><a href="product/${value.PROD_ID}"><img src="${value.PROD_IMG}" alt="${value.PROD_NAME}"></a> </div> <div class="product-info"> <p class="title"><a href="product/${value.PROD_ID}" >${value.PROD_NAME}</a></p> <p class="subtitle">Quantity: ${value.quantity.toString()}</p> <p>Selling Price: Php ${parseFloat(value.PROD_PRICE).toFixed(2)}</p> <p class="subtitle">Sub Total: Php ${value.quantity * parseFloat(value.PROD_PRICE).toFixed(2)}</p> </div> </div> </div>`;            $('#cartProducts').append(sHtml);
        });
    } else {
        $('#btn-checkout').attr('disabled', 'disabled');
    }

    localStorage.setItem('subtotal', total);
    $('#subtotal').text(parseFloat(total).toFixed(2));
    $('#checkout-subtotal').text(parseFloat(total).toFixed(2));

    if (total) {
        $('#total').text(total.toFixed(2));
    }

    var user = JSON.parse(localStorage.getItem('user'));
    if (user) {
        let biller = user.biller;
        let shipper = user.shipper;
        var sHtml = `<p class="subtitle"><b>Biller's Name:</b> ${biller.name}</p><p class="subtitle"><b>Email Address:</b> ${biller.email}</p><p class="subtitle"><b>Home Address:</b> ${biller.address}, ${biller.city}, ${biller.province}</p><p class="subtitle"><b>Phone Number:</b> ${biller.phone}</p>`;

        var sHtml2 = `<p class="subtitle"><b>Shipper's Name:</b> ${shipper.name}</p><p class="subtitle"><b>Shipping Address:</b> ${shipper.address}, ${shipper.city}, ${shipper.province}</p>`;

        $('.billing-content').html(sHtml);
        $('.shipping-content').html(sHtml2);

        $('#shipping_rate').text(user.shipper.rate);
        var total = 0;
        var subtotal =  parseFloat(localStorage.getItem('subtotal'));
        total = subtotal + parseFloat(user.shipper.rate);
        if (total) {
            $('#total').text(total.toFixed(2));
            $('#amount').val(total.toFixed(2));
            $('#rate').val(user.shipper.rate);
            $('#user_info').val(JSON.stringify(user));
            $('#item_list').val(localStorage.getItem('product'));
        }
    }

    var reserve = localStorage.getItem('reserve');
    if (reserve) {
        var reservation_details = JSON.parse(reserve);
        $('#book-amount-display').text(reservation_details.amount);
        $('#reserve-amount').val(reservation_details.amount);
        $('#reserve_list').val(reserve);

        var item = JSON.parse(reservation_details.item);
        $('#reserveImage').attr('src', item.image);
        $('#reserveName').text(item.name);
        $('#reserveDate').text(reservation_details.reserve_date);
        $('#reserve_item_info').val(reserve);
        if (reservation_details.type == 'service') {
            $('#serv_id').val(reservation_details.book_id);
        } else {
            $('#pack_id').val(reservation_details.book_id);
        }

        $('#branch_id').val(reservation_details.branch_id);
        $('#date_reserved1').val(reservation_details.date_reserved);
        $('#time_reserved').val(reservation_details.time_reserved);
    }
});

$('#clear-cart').on('click', function() {
    localStorage.setItem('count', 0);
    localStorage.removeItem('product');
    $('#badge-cart').text('');
    $('#order-item').text('');
    $('#subtotal').text(0.00);
    $('#cartProducts').html('');
});

$('#province').on('change', function() {
    var getRate = parseFloat( $('#province :selected').data('rate')).toFixed(2);
    var rate = getRate.toString();
    $('#shipping_rate').text(rate);
    var total = 0;
    var subtotal =  parseFloat(localStorage.getItem('subtotal'));
    total = subtotal + parseFloat(getRate);
    if (total) {
        $('#total').text(total.toFixed(2));
    }
});


$('#proceed-button').on('click', function(e) {
    // e.preventDefault();
    var data = {
        'biller': {
            'name'     : $('#biller_name').val(),
            'phone'    : $('#biller_phone').val(),
            'address'  : $('#biller_address').val(),
            'city'     : $('#biller_city').val(),
            'country'  : 'PH',
            'postal'   : $('#biller_postal').val(),
            'email'    : $('#email').val(),
            'province' : $('#biller_province :selected').val(),
        },
        'shipper': {
            'name'     : $('#shipper_name').val(),
            'address'  : $('#shipper_address').val(),
            'city'     : $('#shipper_city').val(),
            'country'  : 'PH',
            'postal'   : $('#shipper_postal').val(),
            'province' : $('#shipper_province :selected').val(),
            'rate'     : $('#shipper_province :selected').data('rate')
            }
    };
    localStorage.setItem('user', JSON.stringify(data));
});

$('#paypal-button').on('click', function() {

    var user = JSON.parse(localStorage.getItem('user'));
    var total = 0;
    total = parseFloat(localStorage.getItem('subtotal')) + parseFloat(user.shipper.rate);
    $(this).text('Please wait ..');
    $(this).attr('disabled', 'disabled');
    axios({
      method: 'POST',
      url: `/api/payment/paypal`,
      data: {
        items: JSON.parse(localStorage.getItem('product')),
        shipping: parseFloat(user.shipper.rate),
        amount: parseFloat(total),
        subtotal: parseFloat(localStorage.getItem('subtotal')),
        user: JSON.parse(localStorage.getItem('user'))
      }
    })
    .then(function (response) {
        window.location.href = response.data;
    })
    .catch(function (error) {
        console.log(error);
    });
});


$('#paypal-reserve').on('click', function() {

    var reserve = localStorage.getItem('reserve');
    var reservation_details = JSON.parse(reserve);
    var item = JSON.parse(reservation_details.item);
    $(this).text('Please wait ..');
    $(this).attr('disabled', 'disabled');
    axios({
      method: 'POST',
      url: `/api/reservation/paypal`,
      data: {
        amount: reservation_details.amount,
        name: item.name,
        subtotal: parseFloat(localStorage.getItem('subtotal')),
        reservation: localStorage.getItem('reserve'),
        pet: localStorage.getItem('pet_information')
      }
    })
    .then(function (response) {
        window.location.href = response.data;
    })
    .catch(function (error) {
        console.log(error);
    });
});


$('#btn-feedback').on('click', function() {
    var id = $(this).data('id');
    $('#product_id').val(id);

});

function checkPwd(str) {
    if (str.length < 6) {
        return("too_short");
    } else if (str.length > 50) {
        return("too_long");
    } else if (str.search(/\d/) == -1) {
        return("no_num");
    } else if (str.search(/[a-zA-Z]/) == -1) {
        return("no_letter");
    } else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {
        return("bad_char");
    }
    return(true);
}


$('#frm-password').on('submit', function() {
    if ($('#pp_confirm_password').val() !== $('#pp_new_password').val()) {
        alert('Confirm password does not match to your new password');
        return false;
    } else {
        var a = checkPwd($('#pp_new_password').val());
        if (a != true) {
            alert('Password must atleast hav 6 characters, 1 capital letter, 1 small letter, 1 number and does not contain any special characters.');
            return false;
        }
        return true;
    }
});

$('.frm-login').on('submit', function() {
    var email = $('#email').val();
    var storeEmail = localStorage.getItem('email');
    var loginAttempt = localStorage.getItem('attempts');

    if (loginAttempt == '' || loginAttempt == null || !loginAttempt) {
        loginAttempt = 1;
    }

    if (email == storeEmail) {
        loginAttempt = parseInt(loginAttempt) + 1;
    }

    localStorage.setItem('email', email);
    localStorage.setItem('attempts', loginAttempt);

    return true;
});

$('#frm-reset').on('submit', function() {
    if ($('#rr_confirm_password').val() !== $('#rr_new_password').val()) {
        alert('Confirm password does not match to your new password');
        return false;
    } else {
        var a = checkPwd($('#rr_new_password').val());
        if (a != true) {
            alert('Password must atleast hav 6 characters, 1 capital letter, 1 small letter, 1 number and does not contain any special characters.');
            return false;
        }
        return true;
    }
});

$('#same').on('change', function() {
    if(this.checked) {
        $('#shipper_name').val($('#biller_name').val());
        $('#shipper_name').attr('disabled', 'disabled');

        $('#shipper_address').val($('#biller_address').val());
        $('#shipper_address').attr('disabled', 'disabled');

        $('#shipper_postal').val($('#biller_postal').val());
        $('#shipper_postal').attr('disabled', 'disabled');

        $('#shipper_province').val($('#biller_province :selected').val());
        $('#shipper_province').attr('disabled', 'disabled');

        $('#shipper_city').val($('#biller_city').val());

        $('#shipper_city_select').html('<option value="  ' +  $('#biller_city_select :selected').val()  + ' ">' + $('#biller_city_select :selected').val() + '</option>');
        $('#shipper_city_select').attr('disabled', 'disabled');
    } else {
        $('#shipper_name').removeAttr('disabled');
        $('#shipper_address').removeAttr('disabled');
        $('#shipper_postal').removeAttr('disabled');
        $('#shipper_province').removeAttr('disabled');
        $('#shipper_city_select').removeAttr('disabled');
    }

});
