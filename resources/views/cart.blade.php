@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="cartProducts">
            </div>
            <div class="col-sm-12 col-xs-12 col-md-4 col-lg-4">
                <p class="title">Thanks for shopping at Animal Shop!</p>
                <p>Before you checkout, please make sure you have thoroughly read our <a href="{{route('privacy')}}">Return and refund Policy</a> and <a href="{{route('terms')}}">Terms and conditions</a></p>
                <p class="title">Sub Total amount: <b style="color: #3097D1">Php <span id="subtotal">0.00</span></b></p>
                @if(Auth::check())
                    <button id="btn-checkout" class="btn btn-cart btn-primary btn-lg"><i class="fa fa-cart-arrow-down"></i> CHECKOUT</button>
                @else
                     <a href="{{route('login')}}" class="btn btn-login-checkout btn-cart btn-primary btn-lg">LOGIN TO PROCEED CHECKOUT</a>
                @endif
                <a href="{{route('index')}}" class="btn btn-cart btn-success btn-lg"><i class="fa fa-cart-plus"></i> CONTINUE SHOPPING</a>
                <button class="btn btn-cart btn-default btn-lg" id="clear-cart"> <i class="fa fa-trash"></i> CLEAR CART</button>
            </div>
        </div>
    </div>
@endsection