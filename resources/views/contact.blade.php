@extends('layouts.landing')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <p class="header-title">Contact Us</p>
            <p class="text-center">Need help on your order or booking? Send us a message.</p>
            <br>
            @if (count($errors))
                <div class="alert alert-danger" role="alert">
                    <span>{{ $errors->first('CUST_EMAIL') }}</span>
                </div>
            @endif

            @if(session('message'))
                <div class="alert alert-info" role="alert">
                    <span>{{ session('message') }}</span>
                </div>
            @endif
            <form action="{{ route('api.message')}}" method="POST">
                <input required type="email" name="email" class="form-control input" placeholder="Email address">
                <br>
                <input required type="name" name="name" class="form-control input" placeholder="Name">
                <br>
                <textarea required name="message" id="" cols="30" rows="10" class="form-control" placeholder="Write your message here."></textarea>
                <br>
                <input type="submit" class="btn btn-primary btn-lg pull-right" value="Send message">
            </form>
        </div>
    </div>
</div>
@endsection