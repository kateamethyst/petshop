@extends('layouts.app')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="hidden-xs hidden-sm col-md-6 col-lg-8 promotion">
                <a class="logo" href="{{ route('index') }}"><img src="/img/logo2.png" alt="Logo"></a>
                <div class="quote">
                    <p class="title">Great Deals.</p>
                    <p class="title">Unbeatable value.</p>
                    <div class="links">
                        <a href="{{route('about')}}" class="btn-link">About</a>
                        <a href="{{route('index')}}" class="btn-link">Continue Shopping</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-4 signup">
                <div class="content">
                    <p class="title">Login</p>
                    <div class="login-message"></div>
                    <form action="{{ route('login')}}" method="POST" class="form-signup frm-login">
                        <div class="login-error"></div>

                        @if(session('message'))
                            <div class="alert alert-info" role="alert">
                                <span>{{ session('message') }}</span>
                            </div>
                        @endif
                        {{ csrf_field() }}
                        <label for="username">Email Address: </label>
                        <input name="CUST_EMAIL" type="text" required class="input form-control" id="email" value="">
                        @if ($errors->has('CUST_EMAIL'))
                                <span class="help-block" style="color: red;">
                                    <strong>
                                        @if (strpos($errors->first('CUST_EMAIL'), 'Too many login attempts.') !== false)
                                        @else
                                            {{ $errors->first('CUST_EMAIL') }}
                                        @endif
                                    </strong>
                                </span>
                            @endif

                        <label for="password">Password: </label>
                        <input name="CUST_PASSWORD" required type="password" class="input form-control">

                        <a href="{{ route('forgot') }}" class="forgot">Forgot your password?</a>
                        <input type="submit" value="Login" id="login-button" class="btn btn-primary form-control btn-default is-caps">
                        <p>Not yet a member? Sign up <a href="{{route('register')}}">here</a>.</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection