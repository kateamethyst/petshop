<!DOCTYPE html>
<html lang="en">
    <head>
        <title>AnimalPetshop | Welcome Email</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body style="background-color: #fff;">
        <div class="container" style="margin-top:50px;">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img style="width: 200px; margin:0 auto;" class="img-responsive" title="Animal Petshop" src="https://i.imgur.com/7x1rJPB.png" alt="Animal Petshop">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-12 text-center">
                    <h2>Welcome Aboard <strong>{{ $name }}</strong></h2>
                    <p>Here is your system generated password: <strong>{{ $password }}</strong></p>
                    <a target="_blank" href="{{ route('login') }}" class="btn btn-primary btn-lg">Get Started</a>
                    <br><br>
                    <p>Thanks for signing up -exclusive offers and Pet products new is heading your way.</p>
                    <p>Let the fun begin.</p>
                    <a class="btn btn-lg btn-primary" href="{{ route('index') }}" title="Shop Now">Shop Now</a>
                    <br><br>
                    <p>If you did not do this action, you can safely ignore this email.</p>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <p>This message was sent by Animal Petshop</p>
                    <p>121 Paso de Blas, Valenzuela City (across CVC in Malinta ExitTollgate) </p>
                    <p>Tel. No.(02) 2927255</p>
                </div>
            </div>
        </div>
    </body>
</html>
