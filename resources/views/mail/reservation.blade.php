<!DOCTYPE html>
<html lang="en">
    <head>
        <title>AnimalPetshop | Reservation Email</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        <style type="text/css">
            .table td, .table th {
                border: none;
            }
        </style>
    </head>
    <body style="background-color: #fff;">

        @if (strpos(Route::currentRouteName(), 'print.reservation') === 0 )
            <button onClick="window.print()" style="padding: 8px 30px; background: #013465; outline: none; cursor: pointer; border: 0px; color: white;">Print Reservation</button>
        @endif

        <div class="container" style="margin-top:10px;">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img style="width: 200px;" class="img-responsive" titles="Animal Petshops" src="https://i.imgur.com/7x1rJPB.png" alt="Animal Petshop">
                </div>
            </div>
        </div>

        <div class="container" style="margin-top:30px;">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                     <div class="table-responsive">
                        <table class="table" style="width: 100%;" border="0">
                            <tbody>
                                <tr style="border: none;">
                                    <td style="border: none; width: 50%;"><h3><strong>Your Reservation is Confirmed!</strong></h3></td>
                                </tr>
                                <tr style="border: none;">
                                    <td style="border: none; width: 50%;"><p><strong>Reservation Number</strong>: {{ $id }}</p></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                     <div class="table-responsive">
                        <table class="table table-bordered" style="width: 100%;" >
                            <tbody>
                                <tr style="background: #2c2c2c; color: #fff;">
                                    <td style="width: 20%"><strong>Reservation Details</strong></td>
                                    <td style="width: 80%"></td>
                                </tr>
                                <tr>
                                    <td><strong>Customer Name</strong></td>
                                    <td>{{ $customer_name }}</td>
                                </tr>

                                <tr >
                                    <td><strong>Service Availed</strong></td>
                                    <td>{{ $name }}</td>
                                </tr>

                                <tr>
                                    <td><strong>Service Details</strong></td>
                                    <td>{{ $details }}</td>
                                </tr>

                                <tr>
                                    <td><strong>Reservation Date</strong></td>
                                    <td>{{ $date }}</td>
                                </tr>

                                <tr>
                                    <td><strong>Branch and Location</strong></td>
                                    <td>{{ $branch }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                     <div class="table-responsive">
                        <table class="table table-bordered" style="width: 100%;" >
                            <tbody>
                                <tr style="background: #2c2c2c; color: #fff;">
                                    <td style="width: 20%"><strong>Pet Details</strong></td>
                                    <td style="width: 80%"></td>
                                </tr>
                                <tr>
                                    <td><strong>Pet Name</strong></td>
                                    <td>{{ $pet_name }}</td>
                                </tr>

                                <tr >
                                    <td><strong>Pet Color</strong></td>
                                    <td>{{ $color }}</td>
                                </tr>

                                <tr>
                                    <td><strong>Age</strong></td>
                                    <td>{{ $age }} year/s old</td>
                                </tr>

                                <tr>
                                    <td><strong>Category</strong></td>
                                    <td>{{ $pet_category }}</td>
                                </tr>

                                <tr>
                                    <td><strong>Breed</strong></td>
                                    <td>{{ $breed }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <p>For more information, please read our <a target="_blank" href="{{route('terms')}}" title="Terms and Condition">Terms and Condition</a></p>
                    <p>121 Paso de Blas, Valenzuela City (across CVC in Malinta ExitTollgate) </p>
                    <p>Please <a href="{{route('print.reservation', ['id' => $id])}}">print out</a> or <a href="{{route('pdf.reservation', ['id' => $id])}}">download</a> this receipt as a proof of reservation. </p>
                    <p>Tel. No.(02) 2927255</p>
                </div>
            </div>
        </div>

    </body>
</html>
