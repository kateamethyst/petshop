<!DOCTYPE html>
<html lang="en">
<head>
    <title>AnimalPetshop | Invoice Email</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        .table td, .table th {
            border: none;
        }
    </style>
</head>
<body style="background: #fff;">
    @if (strpos(Route::currentRouteName(), 'print.orders') === 0 )
    <button onClick="window.print()" style="padding: 8px 30px; background: #013465; outline: none; cursor: pointer; border: 0px; color: white;">Print Invoice</button> @endif
    <div class="container" style="margin-top:50px;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <img style="width: 200px;" class="img-responsive" title="Animal Petshops" src="https://i.imgur.com/7x1rJPB.png" alt="Animal Petshop">
            </div>
        </div>
    </div>
    <div class="container" style="margin-top:30px;">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                 <div class="table-responsive">
                    <table class="table" style="width: 100%;" border="0">
                        <tbody>
                            <tr style="border: none;">
                                <td style="border: none; width: 50%;"><p><strong>Invoice</strong>: {{ $id }}</p></td>
                                <td style="border: none; width: 50%;"> <p><strong>Order Date:</strong> {{ date('M d, Y ', strtotime($date_purchased)) }} </p></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="table-responsive">
                    <table class="table" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td style=" border: none; "><strong>Billing Details</strong></td>
                                <td style="border: none; "><strong>Shipping Details</strong></td>
                            </tr>
                            <tr>
                                <td style="border: none;"> {{ $biller_name }} </td>
                                <td style="border: none;"> {{ $shipper_name }} </td>
                            </tr>
                            <tr>
                                <td style="border: none;">{{ $biller_address }}</td>
                                <td style="border: none;">{{ $shipper_address }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="table-responsive">
                    <br>
                    <table class="table table-bordered table-striped" style="width: 100%;">
                        <thead>
                            <tr style="background: #2c2c2c; color: #fff;">
                                <th></th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>
                                        @if ($product->image)
                                            <img style="width: 50px; height: 50px; object-fit: cover;" src="{{'data:image/jpeg;base64,' . base64_encode($product->image)}}" alt="">
                                        @else
                                            <img style="width: 50px; height: 50px; object-fit: cover;" src="/img/empty-product-large.png" alt="">
                                        @endif
                                    </td>
                                    <td>{{ $product->name}}</td>
                                    <td>{{$product->quantity}}</td>
                                    <td>Php {{$product->price}}</td>
                                    <td class="text-right">Php {{number_format(($product->quantity * $product->price), 2)}}</td>
                               </tr>
                            @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><strong>Subtotal</strong></td>
                                    <td class="text-right">Php {{number_format(($total - $rate), 2)}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><strong>Shipping + Handling</strong></td>
                                    <td class="text-right">Php {{$rate}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><strong>Total</strong></td>
                                    <td class="text-right"><strong>Php {{number_format($total, 2)}}</strong></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <p>This message was sent by Animal Petshop</p>
                <p>For more information, please read our <a target="_blank" href="{{route('terms')}}" title="Terms and Condition">Terms and Condition</a></p>
                <p>121 Paso de Blas, Valenzuela City (across CVC in Malinta ExitTollgate) </p>
                <p>Please <a href="{{route('print.orders', ['id' => $id])}}">print out</a> or <a href="{{route('pdf.orders', ['id' => $id])}}">download</a> this receipt as a proof of order. </p></p>
                <p>Tel. No.(02) 2927255</p>
            </div>
        </div>
    </div>
</body>
</html>
