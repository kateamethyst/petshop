@extends('layouts.app')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="hidden-xs hidden-sm col-md-6 col-lg-8 promotion">
                <a class="logo" href="{{ route('index') }}"><img src="/img/logo2.png" alt="Logo"></a>
                <div class="quote">
                    <p class="title">Great Deals.</p>
                    <p class="title">Unbeatable value.</p>
                    <div class="links">
                        <a href="{{route('about')}}" class="btn-link">About</a>
                        <a href="{{route('index')}}" class="btn-link">Continue Shopping</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-4 signup">
                <div class="content">
                    <p class="title">Register</p>
                    @if(session('messageError'))
                        <div class="alert alert-danger" role="alert">{{session('messageError') }}</div>
                    @endif

                    @if(session('message'))
                        <div class="alert alert-info" role="alert">{{session('message') }}</div>
                    @endif
                    <form action="{{ route('user.registration') }}" method="POST" class="form-signup">
                        {{ csrf_field() }}
                        <label for="fname">First Name </label>
                        <input required name="first_name" type="text" required class="input form-control" id="fname" value="{{ old('first_name')}}">

                         <label for="last_name">Last Name</label>
                        <input required name="last_name" type="text" required class="input form-control" id="last_name" value="{{ old('last_name')}}">

                         <label for="phone">Phone Number: </label>
                        <input required name="phone" type="text" required class="input form-control" id="phone" value="{{ old('phone')}}">

                        <label for="email">Email Address: </label>
                        <input required name="email" type="text" required class="input form-control" id="email" value="{{ old('email')}}">

                        <label for="username">Username: </label>
                        <input required name="username" type="text" required class="input form-control" id="username" value="{{ old('username')}}">

                        <p></p>
                        <a href="{{route('forgot')}}" class="forgot">Forgot your password?</a>
                        <br>
                        <p></p>
                        <input type="submit" value="Signup" class="btn btn-primary form-control btn-default is-caps">
                        <p>Already have an account? Login <a href="{{route('login')}}">here</a>.</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection