@extends('layouts.landing')

@section('content')
    <div class="container product-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 prod-image">
                <img src="{{'data:image/jpeg;base64,' . base64_encode($product->PROD_IMG)}}" alt="">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 product-info">
                <p class="title prod-name">{{$product->PROD_NAME}}</p>
                <p><a href="{{ route('products', ['category' => $product->category['CAT_ID']]) }}">{{ $product->category['CAT_NAME'] }}</a></p>
                <p class="price">Price: Php. {{$product->PROD_SELLPRICE}}</p>
                <p>Available Quantity: {{ $product->inventory}}</p>
                <input type="number" value="1" class="input input-lg form-control" name="quantity" min="1" id="quantity" data-inventory="{{$product->inventory}}" max="90" style="width: 200px; margin-bottom: 20px;" placeholder="Quantity">
                <button id="btn-add-cart" data-product="{{ json_encode(['inventory' => $product->inventory ,'PROD_ID' => $product->PROD_ID, 'PROD_NAME' => $product->PROD_NAME, 'PROD_PRICE' => $product->PROD_SELLPRICE, 'PROD_IMG' => 'data:image/jpeg;base64,' . base64_encode($product->PROD_IMG)]) }}" class="btn btn-primary btn-lg btn-add-cart"><i class="fa fa-shopping-cart"></i> Add To Cart</button>
                <a href="{{ route('cart') }}" class="btn btn-default btn-lg btn-my-cart">My Cart</a>

                <p class="title" style="margin-top: 20px;">DESCRIPTION</p>
                <p>{{$product->PROD_DESC}}</p>

                <p class="title">SPECIFICATIONS</p>
                <p>Size: {{$product->PROD_SIZE}}</p>
                <p>Color: {{$product->PROD_COLOR}}</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                @if (!$product->feedbacks->isEmpty())
                    <p class="title" style="background: #5291d0;padding: 8px 16px; color: #fff; margin-top: 30px;">FEEDBACKS</p>
                    @foreach($product->feedbacks as $feedback)
                       <div class="feedback-container" style="margin-top: 8px;padding: 24px;">
                            <p style="color: #023465;" class="title">{{ $feedback['TITLE'] }}</p>
                            <p><i>{{ $feedback['reviewer'] . ' : ' . $feedback['human_date']}}</i></p>
                            <p><i>{{$feedback['DESCRIPTION']}}</i></p>
                       </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <p class="header-title left">FEATURED PRODUCTS</p>
            <div class="product-container">
                @foreach($products as $product)
                    <div class="item">
                        <div class="image">
                            <a href="{{route('product',  ['id' => $product->PROD_ID]) }}"><img src="{{'data:image/jpeg;base64,' . base64_encode($product->PROD_IMG)}}" alt=""></a>
                        </div>
                        <div class="content">
                            <p class="name"><a href="{{route('product',  ['id' => $product->PROD_ID]) }}">{{$product->PROD_BRAND}} : {{$product->PROD_NAME}}</a></p>
                            <p class="price">Php {{$product->PROD_SELLPRICE}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection