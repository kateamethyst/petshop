@extends('layouts.landing')

@section('content')
    <div class="banner-container container-fluid">
        <img src="/img/pet-banner.jpg" alt="">
    </div>
    <div class="category-container hidden-xs hidden-sm">
        <ul>
            @foreach($categories as $category)
            <li><a href="{{route('products') . '?category=' .  $category->CAT_ID}}">{{$category->CAT_NAME}}</a></li>
            @endforeach
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                <p class="header-title">LOOKING FOR PET SERVICES?</p>
                <p class="header-content">Animal Shop offers numerous types of services that suitable to all types of dogs and cats.<br>Whether you have a young puppy, a geriatric senior, a Chihuahua, or Great Dane, or anything in between, our Customer Service Representatives will be happy to assist all the options and help you choose the accommodation that is right for your pets.</p>
            </div>
        </div>
        <div class="row">
            @foreach($packages as $package)
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 service">
                <a href="{{route('package',  ['id' => $package->PACK_ID]) }}">
                    @if ($package->PACK_IMG)
                        <img src="{{'data:image/jpeg;base64,' . base64_encode($package->PACK_IMG)}}" alt="">
                    @else
                        <img src="/img/empty-product-large.png" alt="">
                    @endif
                </a>
                <div class="details">
                    <div class="icon"><i class="fa fa-2x fa-user"></i></div>
                    <ul>
                        <li ><p class="title"><a href="{{route('package',  ['id' => $package->PACK_ID]) }}">{{ $package->PACK_NAME}}</a></p></li>
                        <li><p class="description">{{ $package->PACK_DETAILS}}</p></li>
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="container-fluid banner">
        <p class="header-title">GET AN APPOINTMENT</p>
        <p class="header-content">Our free appointment scheduler software functions as a virtual assistant in fixing your appointments and making sure that none of your client meetings are missed. This online appointment scheduler is used by the following private sectors that have reported revenue growth</p>
        <div class="link">
            <a href="{{ route('services') }}" class="btn-link">BOOK NOW</a>
        </div>
    </div>
    <div class="container">
                <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p class="header-title">UPDATE FROM OUR SHOP</p>
                <p class="header-content">At Animal Shop, we want to save your time when buying pet food and supplies, that’s why our website is easy to use and we offer a fast, efficient service. We can offer your pets with absolutely everything they need to make their and your life better</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="product-container">
                    @foreach($products as $product)
                        <div class="item">
                            <div class="image">
                                <a href="{{route('product',  ['id' => $product->PROD_ID]) }}"><img src="{{'data:image/jpeg;base64,' . base64_encode($product->PROD_IMG)}}" alt=""></a>
                            </div>
                            <div class="content">
                                <p class="name"><a href="{{route('product',  ['id' => $product->PROD_ID]) }}">{{$product->PROD_BRAND}} : {{$product->PROD_NAME}}</a></p>
                                <p class="price">P {{$product->PROD_SELLPRICE}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="link">
                    <a href="{{ route('products') }}" class="btn-link">VIEW MORE</a>
                </div>
            </div>
        </div>
    </div>
@endsection