@extends('layouts.landing')

@section('content')
    <div class="container product-header">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 prod-image">
                <img src="{{'data:image/jpeg;base64,' . base64_encode($package->PACK_IMG)}}" alt="">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 product-info">
                <p class="title prod-name">{{$package->PACK_NAME}}</p>
                <p><a href="{{ route('packages') }}">Packages
                </a></p>
                <input type="hidden" name="book-amount" id="book-amount" value="{{$package->PACK_FEE}}">
                <label>Details</label>
                <p>{{$package->PACK_DETAILS}}</p>
                <label>Duration: </label>
                <p>{{ $package->range}}</p>
                <br>
                <label for="">Select Branch:</label>
                <input type="hidden" id="type" value="package">
                <input type="hidden" value="{{ json_encode([
                    'name' => $package->PACK_NAME,
                    'details' => $package->PACK_DETAILS,
                    'amount' => $package->PACK_FEE,
                    'duration' => $package->range,
                    'time_duration' => $package->time_duration,
                    'image' => 'data:image/jpeg;base64,' . base64_encode($package->PACK_IMG)
                    ]) }}" name="book_details" id="book-item-details">
                <input type="hidden" id="book-id" value="{{ $package->PACK_ID}}">
                <select name="branch" id="branch" class="form-control">
                    @foreach($branches as $branch)
                        <option value="{{$branch->BRANCH_ID}}">{{ $branch->BRANCH_NAME . ' : ' . $branch->BRANCH_ADDRESS}}</option>
                    @endforeach
                </select>
                <br>
                <label for="">Choose Date and time:</label>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker6'>
                        <input type='text' name="date_reserved" disbaled id="date_reserved" class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker7'>
                        <input type='text' class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                <br>
                @if(Auth::check())
                <button id="btn-book" class="btn btn-lg btn-primary">BOOK NOW</button>
                @else
                    <a href="{{ route('login')}}" class="btn btn-lg btn-primary">LOGIN TO BOOK THIS PACKAGE</a>
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                @if (!$package->feedbacks->isEmpty())
                    <p class="title" style="background: #5291d0;padding: 8px 16px; color: #fff; margin-top: 30px;">FEEDBACKS</p>
                    @foreach($package->feedbacks as $feedback)
                       <div class="feedback-container" style="margin-top: 8px;padding: 24px;">
                            <p style="color: #023465;" class="title">{{ $feedback['TITLE'] }}</p>
                            <p><i>{{ $feedback['reviewer'] . ' : ' . $feedback['human_date']}}</i></p>
                            <p><i>{{$feedback['DESCRIPTION']}}</i></p>
                       </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <p class="header-title left">FEATURED PACKAGES</p>
                <div class="product-container">
                @foreach($packages as $package)
                    <div class="item">
                        <div class="image">
                            <a href="{{route('product',  ['id' => $package->PACK_ID]) }}"><img src="{{'data:image/jpeg;base64,' . base64_encode($package->PACK_IMG)}}" alt=""></a>
                        </div>
                        <div class="content">
                            <p class="name"><a href="{{route('package',  ['id' => $package->PACK_ID]) }}">{{$package->PACK_NAME}}</a></p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker6').datetimepicker({
                    minDate:new Date(),
                    format: 'MM/DD/YYYY',
                }).on('keypress keyup keydown paste', function (e) {
                    e.preventDefault();
                    return false;
                });;

                $('#datetimepicker7').datetimepicker({
                    format: 'LT',
                }).on('keypress keyup keydown paste', function (e) {
                    e.preventDefault();
                    return false;
                });;
            });
        </script>
@endsection