@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li class="active">Services</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="hidden-xs hidden-sm col-md-2 col-lg-2">
                <p class="title">CATEGORIES</p>
                <ul class="category">
                <li><a href="{{ route('products') }}">All Products</a></li>
                    @foreach($categories as $category)
                        <li><a href="{{ route('products',  ['category' => $category->CAT_ID]) }}">{{$category->CAT_NAME}}</a></li>
                    @endforeach
                    <li><a href="{{ route('services') }}">Services</a></li>
                    <li><a href="{{ route('packages') }}">Packages</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <div class="search-query">
                    <h2>Services</h2>
                    <p>{{ $count }} services found.</p>
                </div>
                <div id="product-result"  class="product-container">
                    @foreach($services as $service)
                    <div class="item">
                        <div class="image">
                            <a href="{{route('service',  ['id' => $service->SERV_ID]) }}">
                                @if ($service->SERV_IMG)
                                    <img src="{{'data:image/jpeg;base64,' . base64_encode($service->SERV_IMG)}}" alt="">
                                @else
                                    <img src="/img/empty-product-large.png" alt="">
                                @endif
                            </a>
                        </div>
                        <div class="content">
                            <p class="name"><a href="{{route('service',  ['id' => $service->SERV_ID]) }}">{{$service->SERV_NAME}}</a></p>
                        </div>
                    </div>
                    @endforeach
                </div>
                 @if (count($services) !== 0)
                    {{ $services->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection