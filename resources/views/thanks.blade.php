@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li class="active">
                        <a href="">Success Order</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 text-center" style="min-height: 450px">
                <img src="/img/logo2.png" alt="" width="20%" height="20%" style="margin-top: 100px">
                <p class="header-title" style="margin-top: 30px">
                    Thank you for shopping with us.
                </p>
                <p class="header-content">We've sent you an email of your order.</p>
                <a href="{{route('index')}}" class="btn btn-primary btn-lg">CONTINUE SHOPPING</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        localStorage.removeItem('count');
        localStorage.removeItem('product');
        localStorage.removeItem('subtotal');
        localStorage.removeItem('user');
        $('#badge-cart').text('');
    });
</script>
@endsection