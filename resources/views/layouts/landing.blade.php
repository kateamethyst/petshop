<!DOCTYPE html>
<html>
    @include('partials.landing-header')
<body>
    @include('partials.landing-navbar')

    <div id="app">
        @yield('content')
    </div>

    @include('partials.landing-footer')

    @include('partials.landing-scripts')

    @yield('scripts')
</body>
</html>