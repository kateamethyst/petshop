<!DOCTYPE html>
<html>
    @include('partials.landing-header')
<body>
    <div id="app">
        @yield('content')
    </div>

    @include('partials.landing-scripts')
    @yield('scripts')
</body>
</html>