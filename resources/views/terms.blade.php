@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p class="header-title">Terms and Conditions</p>
            </div>
        </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <p class="title">Introduction.</p>
                    <br>
                    <p>These terms and conditions contained herein on this webpage, shall govern your use of this website, including all pages within this website. These terms apply in full force and effect to your use of this website and by using this website, you expressly accept all terms and conditions contained herein in full. </p>
                    <br>
                    <p>This website is not for use by any minors (defined as those who are not at least 18 years of age), and you must not use this website if you are a minor.</p>
                    <br>
                    <br>
                    <p class="title">Property Rights.</p>

                    <p>Dr. Esteban’s Animal Shop and/or its licensors own all rights to the intellectual property and material contained in this website, and all such rights are reserved.</p>
                    <br>
                    <p>You are granted a limited license only, subject to the restrictions provided in these terms, for purposes of viewing the material contained on this Website,</p>

                    <br><br>
                    <p class="title">Restrictions.</p>
                    <p>You are expressly and emphatically restricted from all of the following:</p>
                    <ul>
                        <li>publicly performing and/or showing any website material;

                        <li>using this website in any way that is, or may be, damaging to this website;</li>

                        <li>using this website in any way that impacts user access to this website;</li>

                        <li>using this website contrary to applicable laws and regulations, or in a way that causes, or may cause, harm to the website, or to any person or business entity;</li>

                        <li>using this website to engage in any advertising or marketing;</li>
                    </ul>
                    <br>
                    <p>Any user Id/name and password you may have for this Website are confidential and you must maintain confidentiality of such information.</p>
                    <br>
                    <br>
                    <p class="title">No warranties.</p>
                    <p>This website is provided “as is,” with all faults, and Dr. Esteban’s Animal Shop makes no express or implied representations or warranties, of any kind related to this website or the materials contained on this Website. Additionally, nothing contained on this website shall be construed as providing consult or advice to you.</p>
                    <br><br>
                    <p class="title">Limitation of liability.</p>
                    <p>Dr. Esteban’s Animal Shop, including its officers, directors and employees shall not be liable for any indirect, consequential or special liability arising out of or in any way related to your use of this website.</p>
                    <br><br>
                    <p class="title">Indemnification.</p>
                    <br>
                    <p>You hereby indemnify to the fullest extent Dr. Esteban’s Animal Shop from and against any and all liabilities, costs, demands, causes of action, damages and expenses (including reasonable attorney’s fees) arising out of or in any way related to your breach of any of the provisions of these Terms.</p><br><br>

                    <p class="title">Severability.</p>
                    <p>If any provision of these terms is found to be unenforceable or invalid under any applicable law, such unenforceability or invalidity shall not render these terms unenforceable or invalid as a whole, and such provisions shall be deleted without affecting the remaining provisions herein.</p>

                    <br><br>
                    <p class="title">Variation of Terms.</p>
                    <p>Dr. Esteban’s Animal Shop is permitted to revise these terms at any time as it sees fit, and by using this website you are expected to review such terms on a regular basis to ensure you understand all terms and conditions governing use of this website.</p>
                    <br><br>

                    <p class="title">Assignment.</p>

                    <p>Dr. Esteban’s Animal Shop shall be permitted to assign, transfer, and subcontract its rights and/or obligations under these terms without any notification or consent required. However, .you shall not be permitted to assign, transfer, or subcontract any of your rights and/or obligations under these terms.</p><br><br>

                    <p class="title">Entire Agreement.</p>
                    <p>These terms, including any legal notices and disclaimers contained on this website, constitute the entire agreement between Dr. Esteban’s Animal Shop and you in relation to your use of this website, and supersede all prior agreements and understandings with respect to the same.</p>
                    <br><br>
                    <p class="title">Governing Law & Jurisdiction.</p>
                    <p>These terms will be governed and construed in accordance with the laws of the State, and you submit to the non-exclusive jurisdiction of the state and federal courts located in state for the resolution of any disputes.</p>
                    <br><br>

                    <p class="title">Delivery of items</p>
                    <p>The deliveries are within Valenzuela city and Bulacan which is in Meycauayan and Marilao only, because they have three branches which is in Paso De Blas and Gen T. De Leon, Valenzuela City and in Banga, Meycauayan, Bulacan. </p>
                    <br><br>

                    <p class="title">Return Policy</p>
                    <p>You may return your item if it is defective or incorrect delivery. You can call us or email us at info.animalpetshop@gmail.com for further details of your concern.</p>
                    <br><br>

                    <p class="title">Reservation Policy  </p>
                    <p>Present your reservation slip to the store for the proof of reservation</p>
                    <br><br>

                    <p class="title">Late Policy for Reservation</p>
                    <p>Dr. Esteban’s Animal Shop have First Come, First Serve policy for the customers. If you are late to your schedule of reservation, you must wait until the previous customer is done. However, if there is a previous customer before you, you need to wait him/her to be finished for the operation of service.</p>
                    <br><br>

                    <p class="title">Refund, Cancellation of orders and Re-Schedule of reservation</p>
                    <p>No refund, no re-schedule of reservation and no cancellation of orders.</p>


                </div>
            </div>
    </div>
@endsection