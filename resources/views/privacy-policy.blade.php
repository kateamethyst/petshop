@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p class="header-title">Privacy Policy</p>
            </div>
        </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">

                    <p>This practice is required to provide our clients with notice of our privacy practices with respect to protect personal information collected from this website.</p>
                    <br><br>
                    <p class="title">Consent</p>
                    <p>By using this website, you consent to understand and accept the terms listed in this privacy policy. We reserve the right to update, amend, or change this Privacy Policy at any time to conform to new laws and regulations or changes in business standards. This privacy policy does not extend to any third party websites that link to or from our website and we are not responsible for any of the content on those websites. Those websites have their own privacy policies and it is recommended that users read through them before providing any information to the website owner.</p>
                    <br><br>
                    <p class="title">Information Security</p>
                    <p>Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. All forms have a encryption to securely transmit information.
                    We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These include internal reviews of our data collection, storage and processing practices and security measures, as well as physical security measures to guard against unauthorized access to systems where we store personal data.
                    Our privacy policy does not cover content that is transmitted via email to our office. Email does not have the same encryption and security protocols and should not be used to transmit sensitive information.</p>
                    <br><br>


                    <p class="title">Information Sharing</p>
                    <p>Our monthly newsletter service sends monthly newsletters with relevant information to the topic of interest. Your email will not be sold, rented or leased to a third-party. You may remove your name from the newsletter list at any time by following the removal instructions at the bottom of any newsletter sent.</p>
                    <br><br>
                    <p class="title">Social Media Policy</p>
                    <p>We are not responsible for any information collected by social networks on which we maintain a social media presence. Each social network has its own privacy policy and it should be read before creating an account on the network. We are not responsible for any marketing or retargeting performed by a social network after you have visited our pages.</p>
                </div>
            </div>
    </div>
@endsection