@extends('layouts.app')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="hidden-xs hidden-sm col-md-6 col-lg-8 promotion">
                <a class="logo" href="{{ route('index') }}"><img src="/img/logo2.png" alt="Logo"></a>
                <div class="quote">
                    <p class="title">Great Deals.</p>
                    <p class="title">Unbeatable value.</p>
                    <div class="links">
                        <a href="{{route('about')}}" class="btn-link">About</a>
                        <a href="{{route('index')}}" class="btn-link">Continue Shopping</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-4 signup">
                <div class="content">
                    <p class="title">Choose your new password </p>
                    @if(session('messageError'))
                        <div class="alert alert-danger" role="alert">{{session('messageError') }}</div>
                    @endif

                    @if(session('message'))
                        <div class="alert alert-info" role="alert">{{session('message') }}</div>
                    @endif
                    <form action="{{ route('api.reset-password') }}" id="frm-reset" method="POST" class="form-signup">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ app('request')->input('id') }}">
                        <label for="fname">Password</label>
                        <input required id="rr_new_password" name="password" type="password" required class="input form-control">

                        <label for="last_name">Confirm Password</label>
                        <input required id="rr_confirm_password" name="confirm_password" type="password" required class="input form-control">

                        <input type="submit" value="Reset" class="btn btn-primary form-control btn-default is-caps">

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection