@extends('layouts.app')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="hidden-xs hidden-sm col-md-6 col-lg-8 promotion">
                <a class="logo" href="{{ route('index') }}"><img src="/img/logo2.png" alt="Logo"></a>
                <div class="quote">
                    <p class="title">Great Deals.</p>
                    <p class="title">Unbeatable value.</p>
                    <div class="links">
                        <a href="{{route('about')}}" class="btn-link">About</a>
                        <a href="{{route('index')}}" class="btn-link">Continue Shopping</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-4 signup">
                <div class="content">
                    <p class="title">Reset your password?</p>
                    <p>Please provide the email address that you used when you signed up for your AnimalPetshop account.

                We will send you an email that will allow you to reset your password.</p>
                    <form action="{{ route('api.reset')}}" method="POST" class="form-signup">
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                <span>{{ session('error') }}</span>
                            </div>
                        @endif
                        @if (session('message'))
                            <div class="alert alert-info" role="alert">
                                <span>{{ session('message') }}</span>
                            </div>
                        @endif
                        {{ csrf_field() }}
                        <label for="username">Email Address: </label>
                        <input name="email" required type="email" required class="input form-control" id="email" value="">
                        <input type="submit" value="Send verification email" class="btn btn-primary form-control btn-default is-caps">
                        <p>Not yet a member? Sign up <a href="{{route('register')}}">here</a>.</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection