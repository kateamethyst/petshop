<div class="footer">
    <div class="footer-item -logo">
        <img src="{{ asset('img/logo2.png') }}" alt="Logo">
        <p class="title is-caps">Connect with us</p>
        <a href="" class="media-icons"><i class="fa fa-facebook"></i></a>
        <a href="" class="media-icons"><i class="fa fa-twitter"></i></a>
        <a href="" class="media-icons"><i class="fa fa-youtube"></i></a>
        <a href="" class="media-icons"><i class="fa fa-instagram"></i></a>
    </div>
    <div class="footer-item">
        <p class="title is-caps">AnimalShop.com</p>
        <ul>
            <li>
                <a href="{{ route('about') }}">About Us</a>
            </li>
            <li>
                <a href="{{ route('privacy') }}">Privacy Policy</a>
            </li>
            <li>
                <a href="{{ route('terms') }}">Terms and Conditions</a>
            </li>
        </ul>
    </div>
    <div class="footer-item">
        <p class="title is-caps">Let us help you</p>
        <ul>
            <li>
                <a href="{{ route('contact') }}">Contact Us</a>
            </li>
        </ul>
    </div>
</div>

<div class="copy-right">
    <p>© 2018 <span>Animal Shop</span>. All rights reserved.</p>
</div>