<div class="nav-text">
    <ul>
        <li class="{{ strpos(Route::currentRouteName(), 'customer.index') === 0 ? 'active' : '' }}"><a href="{{ route('customer.index') }}">Transactions<span></span></a></li>
        <li  class="{{ strpos(Route::currentRouteName(), 'customer.reservations') === 0 ? 'active' : '' }}"><a href="{{ route('customer.reservations')}}">Reservations</a></li>
        <li  class="{{ strpos(Route::currentRouteName(), 'customer.account') === 0 ? 'active' : '' }}"><a href="{{ route('customer.account')}}">Account</a></li>
    </ul>
</div>