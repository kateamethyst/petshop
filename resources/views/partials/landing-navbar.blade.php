
<div id="snackbar"></div>
<div class="main-nav">
    <div class="container nav-text-container">
        <div class="hidden-xs hidden-sm col-md-3 col-lg-4 contacts">
        </div>
        <div class="hidden-xs hidden-sm col-md-3 col-lg-4 logo">
            <a href="{{ route('index') }}"><img src="/img/logo2.png" alt=""></a>
        </div>
        <div class="hidden-xs hidden-sm col-md-3 col-lg-4 icons">
            <ul>
                <li><a href="{{route('cart')}}">
                    <i class="fa fa-shopping-cart">
                        <span class="badge" id="badge-cart"></span>
                    </i>
                    </a>
                </li>
                @if(Auth::check())
                    <li><a href="{{ route('customer.index') }}">Go to Dashboard</a></li>
                    <li><a href="{{ route('logout') }}">Logout</a></li>
                @else
                    <li><a href="{{ route('view.login') }}">Login</a></li>
                    <li> | <a href="{{ route('register') }}">Signup</a></li>
                @endif
            </ul>
        </div>
    </div>
    <nav class="navbar nav-container navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand hidden-md hidden-lg" href="{{ route('index') }}"><img src="/img/logo2.png" alt=""></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                    <li class="item {{ strpos(Route::currentRouteName(), 'index') === 0 ? 'active' : '' }}"><a href="{{ route('index') }}">Home</a></li>
                    <li class="item {{ strpos(Route::currentRouteName(), 'services') === 0 ? 'active' : '' }}"><a href="{{ route('services') }}">Services</a></li>
                    <li class="item {{ strpos(Route::currentRouteName(), 'packages') === 0 ? 'active' : '' }}"><a href="{{ route('packages') }}">Packages</a></li>
                    <li class="item {{ strpos(Route::currentRouteName(), 'products') === 0 ? 'active' : '' }}"><a href="{{ route('products') }}">Products</a></li>
                    <li class="item hidden-xs hidden-sm {{ strpos(Route::currentRouteName(), 'about') === 0 ? 'active' : '' }}"><a href="{{ route('about') }}">About</a></li>
                    <li class="item hidden-xs hidden-sm {{ strpos(Route::currentRouteName(), 'contact') === 0 ? 'active' : '' }}"><a href="{{ route('contact') }}">Contact Us</a></li>

                    <li class="item hidden-lg hidden-md {{ strpos(Route::currentRouteName(), 'cart') === 0 ? 'active' : '' }}"><a href="{{ route('cart') }}">My Cart</a></li>

                    @if(Auth::check())
                        <li class="item hidden-lg hidden-md"><a href="{{ route('customer.index') }}">Go to Dashboard</a></li>
                        <li class="item hidden-lg hidden-md"><a href="{{ route('logout') }}">Logout</a></li>
                    @else
                        <li class="item hidden-lg hidden-md {{ strpos(Route::currentRouteName(), 'login') === 0 ? 'active' : '' }}"><a href="{{ route('login') }}">Login</a></li>
                        <li class="item hidden-lg hidden-md {{ strpos(Route::currentRouteName(), 'register') === 0 ? 'active' : '' }}"><a href="{{ route('register') }}">Sign Up</a></li>
                    @endif
                </ul>
                <form class="navbar-form navbar-right" action="{{ route('products')}}" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" aria-label="..." placeholder="Search products here ...">
                        <div class="input-group-btn">
                            <input type="submit" value="Search" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </nav>
</div>