@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li>
                        <a href="{{route('checkout')}}">Checkout</a>
                    </li>
                    <li class="active">Billing</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-8 col-lg-8">
                <p class="title">PAYMENT METHOD</p>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#stripe-content"><img src="https://laz-img-cdn.alicdn.com/tfs/TB1vORScdfJ8KJjy0FeXXXKEXXa-80-80.png" alt="">Credit/Debit Card</a></li>
                    <li><a data-toggle="tab" href="#menu1"><img src="https://laz-img-cdn.alicdn.com/tfs/TB1KjNWclHH8KJjy0FbXXcqlpXa-80-80.png" alt="">Paypal</a></li>
                </ul>

                <div class="tab-content">
                    <div id="stripe-content" class="tab-pane fade in active">
                        <img class="payment-icon" src="/img/visa.png" alt="">
                        <img class="payment-icon" src="/img/master.png" alt="">
                        <form action="/api/payment/stripe" method="POST" id="payment-form">
                          <input type="hidden" name="amount" value="" id="amount">
                          <input type="hidden" name="rate" value="" id="rate">
                          <input type="hidden" name="user" value="" id="user_info">
                          <input type="hidden" name="products" value="" id="item_list">

                          <script
                            src="https://checkout.stripe.com/checkout.js" id="power" class="stripe-button"
                            data-key="pk_test_V42g8GnDK9wTzDYhxT3DiSHw"
                            data-amount=""
                            data-name="Animal Petshop"
                            data-description=""
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-currency=""
                            data-locale="auto">
                          </script>
                        </form>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <p>You will be redirected  to Paypal to complete the payment.</p>
                        <button id="paypal-button" class="btn btn-primary btn-lg">Pay Now</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-4 col-lg-4">
                <p class="title">BILLING DETAILS</p>
                <div class="billing-content">
                </div>
                <p class="title">SHIPPING DETAILS</p>
                <div class="shipping-content"></div>

                <p class="title">ORDER SUMMARY</p>
                <div class="flex">
                    <p class="subtitle subtitle-left">Subtotal (<span id="order-item">1</span> items) </p>
                    <p class="subtitle subtitle-right"><b id="checkout-subtotal">Php 1000.00</b></p>
                </div>
                <div class="flex">
                    <p class="subtitle subtitle-left">Shipping Fee</p>
                    <p class="subtitle subtitle-right"><b>Php  </b><b id="shipping_rate">00.00</b></p>
                </div>
                <div class="flex">
                    <h4 class="subtitle subtitle-left">Total</h4>
                    <h4 class="subtitle subtitle-right"><b class="red">Php <b id="total">0.00</b></b></h4>
                </div>
            </div>
        </div>
    </div>
@endsection