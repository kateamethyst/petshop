@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li class="active">Reservation</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-4 col-lg-4" id="reserveItems">
                <p class="title">Pet Informations</p>
                <label for="">Pet Name</label>
                <input type="text" name="pet_name" id="pet_name" class="form-control">
                <br>
                <label for="">Select type of animal</label>
                <select class="form-control" name="pet_category" id="pet_category">
                    @if (!$pet_categories->isEmpty())
                        @foreach ($pet_categories as $pet_category)
                            <option value="{{$pet_category->PET_CATEG_ID}}">{{$pet_category->PET_CATEG_NAME}}</option>
                        @endforeach
                    @endif
                </select>
                <br>
                <label for="">Select Pet Breed</label>
                <select class="form-control" name="breed_id" id="breed">
                    @if (!$breeds->isEmpty())
                        @foreach ($breeds as $breed)
                            <option value="{{$breed->B_ID}}">{{$breed->B_NAME}}</option>
                        @endforeach
                    @endif
                </select>
                <br>
                <label for="">Gender</label>
                <select class="form-control" name="gender" id="pet_gender">
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
                <br>
                <label for="">Pet Color</label>
                <input type="text" id="pet_color" name="markingcolor" class="form-control">
                <br>
                <label for="">Pet Age</label>
                <input type="text" id="pet_age" name="pet_age" class="form-control">
                <br>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <p class="title">Your Reservation Details</p>
                <br>
                <div>
                    <img style="height: 150px; object-fit: cover;" src="" alt="" id="reserveImage">
                </div>
                <br>
                <p class="title" id="reserveName" style="color: #3097D1"></p>
                <p class="title">Date Reserved:</p> <p><span id="reserveDate"></span></p>
                <p class="title">Thanks for booking at Animal Shop!</p>
                <p>Before you book, please make sure you have thoroughly read our  <a href="{{route('privacy')}}">Return and refund Policy</a> and <a href="{{route('terms')}}">Terms and conditions</a>.</p>
                <p>The services and packages price will not be paid online, it will be paid on the branch you set your reservation. Only the reservation fee is being charged.</p>
                <p class="title">Reservation Fee: <b style="color: #3097D1">Php <span>{{$reservation_fee}}</span></b></p>
                @if(Auth::check())
                    <button id="paypal-reserve" class="btn btn-primary ">Pay with Paypal</button>
                    <form action="/api/reservation/stripe" method="POST" id="payment-form" style="display:inline;">
                          <input type="hidden" name="amount" value="" id="reserve-amount">
                          <input type="hidden" name="item_info" value="" id="reserve_item_info">
                          <input type="hidden" name="user" value="{{Auth::user()->CUST_EMAIL}}" id="user_info">
                          <input type="hidden" name="serv_id" value="" id="serv_id">
                          <input type="hidden" name="pack_id" value="" id="pack_id">
                          <input type="hidden" name="branch_id" value="" id="branch_id">
                          <input type="hidden" name="date_reserved" value="" id="date_reserved1">
                          <input type="hidden" name="time_reserved" value="" id="time_reserved">
                          <input type="hidden" name="pet_information" value="" id="pet_information">
                          <script
                            src="https://checkout.stripe.com/checkout.js" id="power" class="stripe-button"
                            data-key="pk_test_V42g8GnDK9wTzDYhxT3DiSHw"
                            data-amount=""
                            data-name="Animal Petshop"
                            data-description=""
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-currency=""
                            data-locale="auto">
                          </script>
                        </form>
                @else
                    <a href="{{route('login')}}" class="btn btn-cart btn-primary btn-lg">LOGIN TO PROCEED ON PAYMENT</a>
                @endif
            </div>
        </div>
    </div>
@endsection