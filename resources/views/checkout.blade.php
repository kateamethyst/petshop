@extends('layouts.landing')

@section('content')
<form action="{{route('billing')}}" method="GET">
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
           <ol class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li class="active">Checkout</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <p class="title">BILLER INFORMATION</p>
            <div class="form-group ">
                <label for="">Email Address <span class="req">*</span></label>
                <input type="email" required id="email" class="form-control" name="email" placeholder="Please enter your email address" value="">
            </div>
            <div class="form-group ">
                <label for="">Full name <span class="req">*</span></label>
                <input type="text" id="biller_name" required class="form-control" name="biller_name" placeholder="First Last" value="">
            </div>
            <div class="form-group ">
                <label for="">Mobile Number <span class="req">*</span></label>
                <input type="text" required id="biller_phone" class="form-control" name="biller_phone" placeholder="Please enter your email address" value="">
            </div>
            <div class="form-group ">
                <label for="">Home Address<span class="req">*</span></label>
                <input type="text" id="biller_address" required class="form-control" name="biller_address" placeholder="Please enter your complete addresss" value="">
            </div>
            <div class="form-group ">
                <input type="hidden" id="biller_city" required class="form-control" name="biller_city" value="">
            </div>
            <div class="form-group ">
                <label for="">Postal Code<span class="req">*</span></label>
                <input type="text" id="biller_postal" required class="form-control" name="biller_postal" placeholder="Please enter your postal" value="">
            </div>
            <div class="form-group ">
                <label for="">Province <span class="req">*</span></label>
                <select name="biller_province" required id="biller_province" class="form-control">
                    <option value="" disabled="disabled" selected="selected">Choose Province</option>
                    @foreach ($provinceShipping as $province)
                    <option value="{{$province->province}}" data-rate="{{$province->shipping_rate}}">{{$province->province}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group ">
                <label for="">City <span class="req">*</span></label>
                <select name="biller_city_select" required id="biller_city_select" class="form-control">
                    <option value="" disabled="disabled" selected="selected">Choose City</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <p class="title">SHIPPING INFORMATION</p>
            <div class="form-group ">
                <input type="checkbox" name="same" id="same">
                <label for="same"> Do you want your home address as your delivery address?</label>
            </div>
            <div class="form-group ">
                <label for="">Full name <span class="req">*</span></label>
                <input type="text" id="shipper_name" required class="form-control" name="shipper_name" placeholder="First Last" value="">
            </div>
            <div class="form-group ">
                <label for="">Shipping Address<span class="req">*</span></label>
                <input type="text" id="shipper_address" required class="form-control" name="shipper_address" placeholder="Please enter your complete addresss" value="">
            </div>
            <div class="form-group ">
                <input type="hidden" id="shipper_city" required class="form-control" name="shipper_city" value="">
            </div>
            <div class="form-group ">
                <label for="">Postal Code<span class="req">*</span></label>
                <input type="text" id="shipper_postal" required class="form-control" name="shipper_postal" placeholder="Please enter your postal" value="">
            </div>
            <div class="form-group ">
                <label for="">Province <span class="req">*</span></label>
                <select name="shipper_province" required id="shipper_province" class="form-control">
                    <option value="" disabled="disabled" selected="selected">Choose Province</option>
                    @foreach ($provinceShipping as $province)
                    <option value="{{$province->province}}" data-rate="{{$province->shipping_rate}}">{{$province->province}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group ">
                <label for="">City <span class="req">*</span></label>
                <select name="shipper_city_select" required id="shipper_city_select" class="form-control">
                    <option value="" disabled="disabled" selected="selected">Choose City</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <p class="title">ORDER SUMMARY</p>
            <div class="flex">
                <p class="subtitle subtitle-left">Subtotal (<span id="order-item">1</span> items) </p>
                <p class="subtitle subtitle-right"><b id="checkout-subtotal">Php 00.00</b></p>
            </div>
            <div class="flex">
                <p class="subtitle subtitle-left">Shipping Fee</p>
                <p class="subtitle subtitle-right"><b>Php  </b><b id="shipping_rate">00.00</b></p>
            </div>
            <div class="flex">
                <h4 class="subtitle subtitle-left">Total</h4>
                <h4 class="subtitle subtitle-right"><b class="red">Php <b id="total">0.00</b></b></h4>
            </div>
            <input type="submit" id="proceed-button" class="form-control btn btn-primary" value="PROCEED TO PAY">
        </div>

    </div>
</div>
</form>
@endsection