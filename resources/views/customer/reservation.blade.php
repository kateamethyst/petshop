@extends('layouts.landing')

@section('content')
    @include('partials.navtext')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li class="active">Reservations</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-create">
                    <p class="form-header">Reservations</p>
                    <div class="form-content">
                        <table class="table table-bordered" id="reservation-table">
                            <thead>
                                <tr>
                                    <th>Reserve ID</th>
                                    <th>Date of Transaction</th>
                                    <th>Amount</th>
                                    <th>Payment Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        localStorage.removeItem('count');
        localStorage.removeItem('product');
        localStorage.removeItem('subtotal');
        localStorage.removeItem('user');
        $('#badge-cart').text('');

        $('#reservation-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! secure_url(URL::route('show.reservations', [], false)); !!}',
            columns: [
                { data: 'RESERVE_ID', name: 'RESERVE_ID' },
                { data: 'date_added', name: 'date' },
                { data: 'AMOUNT', name: 'AMOUNT' },
                { data: 'PAYMENT_TYPE', name: 'PAYMENT_TYPE' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection