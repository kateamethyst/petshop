@extends('layouts.landing')

@section('content')
    @include('partials.navtext')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li><a href="{{route('customer.reservations')}}">Reservations</a></li>
                    <li class="active">Reservation # {{$reservation->RESERVE_ID}}</li>

                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-create">
                    <p class="form-header">Reservation Details</p>
                    <div class="form-content">
                        <p>Date Reserved: <b>{{ date('D, F j, Y h:i:a', strtotime($reservation->START_DATE_RESERVED))}}</b></p>
                        <p>Selected Branch: <b>{{ $reservation->branches['BRANCH_NAME']}}</b></p>
                        <p>Branch Location: <b>{{ $reservation->branches['BRANCH_ADDRESS']}}</b></p>
                        <p>Branch ContactNumber: <b>{{$reservation->branches['BRANCH_CONTACT']}}</b></p>
                    </div>
                    <p class="form-header">Package Availed</p>
                    <div class="form-content">
                        <div class="alert alert-default alert-dismissible alert-cart">
                                @if(isset($reservation->services['SERV_ID']) && !empty($reservation->services['SERV_ID']))
                                <div class="cart-content">
                                    <div class="image">
                                        <a href="{{route('service',  ['id' => $reservation->services['SERV_ID']]) }}">
                                            @if ($reservation->services['image'])
                                                <img src="{{$reservation->services['image']}}" alt="">
                                            @else
                                                <img src="/img/empty-product-large.png" alt="">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="product-info">
                                        <p class="title">
                                            <a href="{{route('service',  ['id' => $reservation->services['SERV_ID']]) }}" >{{ $reservation->services['SERV_NAME'] }}</a>
                                        </p>
                                        <p>Current Price: {{$reservation->services['SERV_FEE']}}</p>

                                        {{-- @if ($transaction->status == 'Completed') --}}
                                            <button type="button" id="btn-feedback" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" data-id="{{$reservation->services['SERV_ID']}}">Write your feedback</button>
                                        {{-- @endif --}}
                                    </div>
                                </div>
                                @else
                                    <div class="cart-content">
                                        <div class="image">
                                            <a href="{{route('service',  ['id' => $reservation->packages['PACK_ID']]) }}">
                                                @if ($reservation->packages['image'])
                                                    <img src="{{$reservation->packages['image']}}" alt="">
                                                @else
                                                    <img src="/img/empty-product-large.png" alt="">
                                                @endif
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <p class="title">
                                                <a href="{{route('package',  ['id' => $reservation->packages['PACK_ID']]) }}" >{{ $reservation->packages['PACK_NAME'] }}</a>
                                            </p>
                                            <p>Current Price: {{$reservation->packages['PACK_FEE']}}</p>

                                            {{-- @if ($transaction->status == 'Completed') --}}
                                                <button type="button" id="btn-feedback" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal" data-id="{{$reservation->packages['PACK_ID']}}">Write your feedback</button>
                                            {{-- @endif --}}
                                        </div>
                                    </div>
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- Modal -->
    <div id="myModal" class="modal fade" data-backdrop="static" data-keyboard="false"  role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('store.feedback') }}" method="POST">
                    @if(isset($reservation->services['SERV_ID']) && !empty($reservation->services['SERV_ID']))
                        <input required type="hidden" name="serv_id" value="{{$reservation->services['SERV_ID']}}" id="serv_id">
                    @else
                        <input required type="hidden" name="pack_id" value="{{$reservation->packages['PACK_ID']}}" id="pack_id">
                    @endif
                    <label for="feedback">Write your review</label>
                    <textarea required name="description" class="input form-control" id="feedback" placeholder="Write your feedback here."></textarea>
                    <br>
                    <label for="feedback">Add Title</label>
                    <input required type="text" name="title" class="input form-control" placeholder="Add your title here.">
                    <br>
                    <input type="submit" value="Save" class="btn btn-primary">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection