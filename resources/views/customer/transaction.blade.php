@extends('layouts.landing')

@section('content')
    @include('partials.navtext')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li><a href="{{ route('customer.index') }}">Transactions</a></li>
                    <li class="active">Transaction # {{$transaction->ID}}</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-create">
                    <p class="form-header">Transaction Details</p>
                    <div class="form-content">
                        <p>Date purchased: <b>{{$transaction->date_added}}</b></p>
                        <p>Total Item : <b>{{ count($transaction->products) }} item/s</b></p>
                        <p class="">Total Amount: <b>Php {{ $transaction->TOTAL }}</b></p>
                        <p class="">Shipping Fee: <b>Php {{ $transaction->SHIPPING_FEE }}</b></p>
                        <p>Payment: <b>{{$transaction->PAYMENT_TYPE}}</b></p>
                        <p>Contact Number: <b>{{$transaction->CONTACT_NUMBER}}</b></p>
                        <p>Notes/Instructions: <b>{{$transaction->NOTES}}</b></p>
                        <p>Status: <b>{{$transaction->STATUS}}</b></p>
                    </div>

                    <p class="form-header">Biller Details</p>
                    <div class="form-content">
                        <p>Biller's Name <b>{{$transaction->BILLER_NAME}}</b></p>
                        <p>Biller's Address : <b>{{ $transaction->BILLER_ADDRESS }}</b></p>
                    </div>

                    <p class="form-header">Shipping Details</p>
                    <div class="form-content">
                        <p>Shipper's Name <b>{{$transaction->SHIPPER_NAME}}</b></p>
                        <p>Shipper's Address : <b>{{ $transaction->SHIPPER_ADDRESS }}</b></p>
                    </div>

                    <p class="form-header">Ordered Products</p>
                    <div class="form-content">
                        @foreach($transaction->products as $product)
                            <div class="alert alert-default alert-dismissible alert-cart">
                                <div class="cart-content">
                                    <div class="image">
                                        <a href="{{route('product',  ['id' => $product->PROD_ID]) }}">
                                            @if ($product->PROD_IMG)
                                                <img src="{{'data:image/jpeg;base64,' . base64_encode($product->PROD_IMG)}}" alt="">
                                            @else
                                                <img src="/img/empty-product-large.png" alt="">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="product-info">
                                        <p class="title">
                                            <a href="{{route('product',  ['id' => $product->PROD_ID]) }}" >{{ $product->PROD_NAME }}</a>
                                        </p>
                                        <p class="subtitle">Ordered Quantity {{$product->pivot->ORDERED_QUANTITY}}</p>
                                        <p>Current Price: Php {{$product->pivot->CURRENT_PRICE}}</p>

                                        {{-- @if ($transaction->status == 'Completed') --}}
                                            <button type="button" id="btn-feedback" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" data-id="{{$product->PROD_ID}}">Write your feedback</button>
                                        {{-- @endif --}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" data-backdrop="static" data-keyboard="false"  role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('store.feedback') }}" method="POST">
                    <input required type="hidden" name="product_id" value="" id="product_id">
                    <label for="feedback">Write your review</label>
                    <textarea required name="description" class="input form-control" id="feedback" placeholder="Write your feedback here."></textarea>
                    <br>
                    <label for="feedback">Add Title</label>
                    <input required type="text" name="title" class="input form-control" placeholder="Add your title here.">
                    <br>
                    <input type="submit" value="Save" class="btn btn-primary">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection