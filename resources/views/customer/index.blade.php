@extends('layouts.landing')

@section('content')
    @include('partials.navtext')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li class="active">Transactions</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-create">
                    <p class="form-header">Transactions</p>
                    <div class="form-content">
                        <table class="table table-bordered responsive" id="transaction-table">
                            <thead>
                                <tr>
                                    <th>Transaction Id</th>
                                    <th>Total</th>
                                    <th>Payment Type</th>
                                    <th>Date Purchased</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        localStorage.removeItem('count');
        localStorage.removeItem('product');
        localStorage.removeItem('subtotal');
        localStorage.removeItem('user');
        $('#badge-cart').text('');

        $('#transaction-table').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: '{!! secure_url(URL::route('transaction', [], false)); !!}',
            columns: [
                { data: 'ID', name: 'ID' },
                { data: 'TOTAL', name: 'TOTAL' },
                { data: 'PAYMENT_TYPE', name: 'PAYMENT_TYPE' },
                { data: 'date_added', name: 'date_purchased' },
                { data: 'STATUS', name: 'STATUS' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection