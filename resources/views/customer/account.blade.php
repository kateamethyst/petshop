@extends('layouts.landing')

@section('content')
    @include('partials.navtext')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 breadcrumbs">
               <ol class="breadcrumb">
                    <li>
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li class="active">Account</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="profile-content">
                    <form action="{{ route('customer.update')}}" method="POST" role="form" class="form-create">
                        {{ csrf_field() }}
                        <p class="form-header">Profile Settings</p>
                        <div class="form-content">
                            <div class="form-group">
                                @if(session('messageError'))
                                    <div class="alert alert-danger" role="alert">{{session('messageError') }}</div>
                                @endif

                                @if(session('message'))
                                    <div class="alert alert-info" role="alert">{{session('message') }}</div>
                                @endif
                                <label for="">First Name</label>
                                <input required name="CUST_FNAME" type="text" class="form-control" placeholder="First Name" value="{{ $user['CUST_FNAME']}}">
                            </div>

                            <div class="form-group">
                                <label for="">Middle Initial</label>
                                <input name="CUST_MI" type="text" class="form-control" placeholder="Middle Initial" value="{{ $user['CUST_MI']}}">
                            </div>

                            <div class="form-group">
                                <label for="">Last Name</label>
                                <input required type="text" name="CUST_LNAME" class="form-control" placeholder="Last Name" value="{{ $user['CUST_LNAME']}}">
                            </div>

                            <div class="form-group">
                                <label for="">Email</label>
                                <input required type="email" name="CUST_EMAIL" class="form-control" placeholder="Email Address" value="{{ $user['CUST_EMAIL']}}">
                            </div>

                            <div class="form-group">
                                <label for="">Mobile Number</label>
                                <input required type="text" name="CUST_CONTACT" class="form-control" placeholder="Mobile Number" value="{{ $user['CUST_CONTACT']}}">
                            </div>

                            <div class="form-group">
                                <label for="">Username</label>
                                <input required type="text" name="CUST_USERNAME" class="form-control" placeholder="Username" value="{{ $user['CUST_USERNAME']}}">
                            </div>

                            <input type="submit" value="Update Profile" class="btn btn-primary">
                        </div>
                    </form>
                    <form id="frm-password" action="{{ route('customer.password') }}" method="POST" role="form" class="form-create">
                        {{ csrf_field() }}
                        <p class="form-header">Password Settings</p>
                        <div class="form-content">
                            @if(session('passwordError'))
                                <div class="alert alert-danger" role="alert">{{session('passwordError') }}</div>
                            @endif

                            @if(session('password'))
                                <div class="alert alert-info" role="alert">{{session('password') }}</div>
                            @endif
                            <div class="form-group">
                                <label for="">Old Password</label>
                                <input required type="password" name="old_password" class="form-control" id="pp_old_password" placeholder="Old Password" >
                            </div>
                            <div class="form-group">
                                <label for="">New Password</label>
                                <input required type="password" class="form-control" id="pp_new_password" name="new_password" placeholder="New Password" >
                            </div>
                            <div class="form-group">
                                <label for="">Confirm Password</label>
                                <input required type="password" class="form-control" id="pp_confirm_password" placeholder="Confirm Password" >
                            </div>
                            <input type="submit" id="btn-password" value="Change Password" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-imageupload">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="file" name="user_image">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-addressupdate">
        <div class="modal-dialog">
            <form action="" method="POST" role="form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>User Address</h4>
                    </div>
                    <div class="modal-body">

                            <div class="form-group">
                                <label for="">Address 1</label>
                                <input type="text" class="form-control" name="address_1">
                            </div>

                            <div class="form-group">
                                <label for="">Address 2</label>
                                <input type="text" class="form-control" name="address_2">
                            </div>

                            <div class="form-group">
                                <label for="">States</label>
                                <select disabled id="states" name="state" class="form-control">

                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection