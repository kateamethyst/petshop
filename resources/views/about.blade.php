@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p class="header-title">About Us</p>
            </div>
        </div>
        @foreach($abouts as $about)
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <p>{{$about->ABOUT_CONTENT}}</p>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15436.605020405299!2d120.98749600000001!3d14.704037000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfe800ac388dc5f!2sDr.+Isagani+Esteban+Animal+Shop+and+Veterinary+Clinic!5e0!3m2!1sen!2sus!4v1526177900955" width="600" height="450" frameborder="0" style="border:0; display:inline;" allowfullscreen></iframe>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <iframe src="https://www.google.com/maps/embed?pb=!4v1526178018529!6m8!1m7!1sMiGRXJqrM0tIccAl8So70w!2m2!1d14.70442045455198!2d120.9872913855006!3f340!4f5!5f1.157797873125308" width="600" height="450" frameborder="0" style="border:0; display: inline;" allowfullscreen></iframe>
            </div>
        </div>
    </div>
@endsection