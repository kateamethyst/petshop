<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('cities', 'PageController@getCities');

Route::post('message', 'UserController@sendMessage')->name('api.message');

Route::prefix('packages')->group(function() {
    Route::get('/', 'PackageController@index')->name('package.index');
    Route::get('/{id}', 'PackageController@show')->name('package.show');
});

Route::prefix('products')->group(function() {
    Route::get('/', 'ProductController@index')->name('product.index');
    Route::get('/{id}', 'ProductController@show')->name('product.show');
    Route::post('/add', 'ProductController@checkoutProducts');
});

Route::prefix('services')->group(function() {
    Route::get('/', 'ServiceController@index')->name('service.index');
    Route::get('/{id}', 'ServiceController@show')->name('service.show');
});

Route::prefix('users')->group(function() {
    Route::post('registration', 'UserController@registration')->name('user.registration');
});

Route::post('reset', 'UserController@forgotPassword')->name('api.reset');
Route::post('reset-password', 'UserController@resetPassword')->name('api.reset-password');


Route::middleware(['auth'])->group(function() {
    Route::prefix('payment')->group(function() {
        Route::post('stripe', 'PaymentController@payWithStripe');
        Route::post('paypal', 'PaymentController@payWithPaypal');
        Route::get('executePayment', 'PaymentController@processPaypal')->name('process-paypal');
    });

    Route::prefix('reservation')->group(function() {
        Route::post('stripe', 'ReservationController@payWithStripe');
        Route::post('paypal', 'ReservationController@payWithPaypal');
        Route::get('executePayment', 'ReservationController@processPaypal')->name('process-reservation-paypal');
        Route::post('check', 'ReservationController@isReserve')->name('check.reservation');
    });

    Route::prefix('feedback')->group(function() {
        Route::post('/', 'FeedbackController@saveFeedback')->name('store.feedback');
    });
    Route::get('transaction', 'TransactionController@ajax')->name('transaction');
    Route::get('reservations', 'ReservationController@ajax')->name('show.reservations');
});

Route::get('reservation', 'ReservationController@checkAvailability');

Route::resource('breeds', 'BreedController');