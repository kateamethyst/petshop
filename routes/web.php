<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('contact', 'contact')->name('contact');
Route::view('cart', 'cart')->name('cart');
Route::view('terms', 'terms')->name('terms');
Route::get('about', 'PageController@about')->name('about');
Route::get('result', 'PageController@result')->name('products');
Route::get('/', 'PageController@home')->name('index');
Route::get('privacy-policy', 'PageController@privacyPolicy')->name('privacy');

Route::get('product/{id}', 'PageController@getProduct')->name('product');
Route::get('services/{id}', 'PageController@getService')->name('service');
Route::get('packages/{id}', 'PageController@getPackage')->name('package');

Route::get('services', 'PageController@services')->name('services');
Route::get('packages', 'PageController@packages')->name('packages');
Route::get('register', 'PageController@register')->name('register');

Route::view('forgot', 'forgot')->name('forgot');
Route::view('reset', 'reset')->name('reset.password');

Route::middleware(['auth'])->group(function() {

    Route::get('reservation/view/{id}', 'ReservationController@viewReservationEmail')->name('print.reservation');
     Route::get('reservation/pdf/{id}', 'ReservationController@pdf')->name('pdf.reservation');

     Route::get('orders/view/{id}', 'AdminController@viewOrderBilling')->name('print.orders');
     Route::get('orders/pdf/{id}', 'AdminController@pdf')->name('pdf.orders');

    Route::get('billing', 'PageController@billing')->name('billing');
    Route::view('thanks', 'thanks')->name('thanks');
    Route::get('checkout', 'PageController@checkout')->name('checkout');

    Route::get('reserve', 'PageController@reserve')->name('reserve');

    Route::prefix('customer')->group(function() {
        Route::get('/', 'AdminController@index')->name('customer.index');
        Route::get('reservations', 'AdminController@reservations')->name('customer.reservations');
        Route::get('account', 'AdminController@account')->name('customer.account');
        Route::post('update-profile', 'UserController@updateProfile')->name('customer.update');
        Route::post('update-password', 'UserController@updatePassword')->name('customer.password');
        Route::get('transaction/{id}', 'AdminController@viewTransaction')->name('customer.view.transaction');

        Route::get('reservation/{id}', 'AdminController@viewReservation')->name('customer.view.reservation');


    });
});

Route::view('login', 'login')->name('view.login');
Route::post('login', 'AuthController@login')->name('login');
Route::get('logout', 'AuthController@logout')->name('logout');

