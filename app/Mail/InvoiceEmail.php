<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.invoice')->with([
            'id'              => $this->data['id'],
            'products'        => $this->data['products'],
            'date_purchased'  => $this->data['date_purchased'],
            'shipper_address' => $this->data['shipper_address'],
            'biller_address'  => $this->data['biller_address'],
            'biller_name'     => $this->data['biller_name'],
            'shipper_name'    => $this->data['shipper_name'],
            'total'           => $this->data['total'],
            'rate'            => $this->data['rate']
        ]);
    }
}
