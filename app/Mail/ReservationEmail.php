<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReservationEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.reservation')->with([
            'id'            => $this->data['id'],
            'name'          => $this->data['name'],
            'branch'        => $this->data['branch'],
            'date'          => $this->data['date'],
            'details'       => $this->data['details'],
            'customer_name' => $this->data['customer_name'],
            'pet_name'      => $this->data['pet_name'],
            'pet_category'  => $this->data['pet_category'],
            'breed'         => $this->data['breed'],
            'age'           => $this->data['age'],
            'gender'        => $this->data['gender'],
            'color'         => $this->data['color']
        ]);
    }
}
