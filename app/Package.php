<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    /**
     * The table that is related to model
     *
     * @var string
     */
    protected $table = 'package';

    protected $primaryKey = 'PACK_ID';

    protected $appends = [
        'range',
        'image',
        'time_duration'
    ];

    protected $hidden = [
        'PACK_IMG'
    ];

    public function getRangeAttribute()
    {
        $time = explode('.', $this->attributes['PACK_DURATION']);
        $minutes = 60*($time[1]/10)*.10;
        return $time[0] .' hour, ' . $minutes . ' mins';
    }

    public function getTimeDurationAttribute()
    {
        $time = explode('.', $this->attributes['PACK_DURATION']);
        $minutes = 60*($time[1]/10)*.10;
        return $time[0] .':' . $minutes;
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation', 'PACK_ID');
    }

    /**
     * Convert blob to base64
     *
     * @return string
     */
    public function getImageAttribute()
    {
        return 'data:image/jpeg;base64,' . base64_encode($this->PACK_IMG);
    }

    public function feedbacks()
    {
        return $this->hasMany('App\Feedback', 'PACK_ID');
    }
}
