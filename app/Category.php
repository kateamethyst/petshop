<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The table that is related to model
     *
     * @var string
     */
    protected $table = 'category';

    protected $primaryKey = 'CAT_ID';

    public function products()
    {
        return $this->hasMany('App\Product', 'CAT_ID');
    }
}
