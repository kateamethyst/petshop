<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    protected $table = 'breed';

    protected $primaryKey = 'B_ID';

    public function pet_category()
    {
        return $this->belongsTo('App\PetCategory', 'PET_CATEG_ID');
    }
}
