<?php

namespace App;

use App\Category;
use App\ProductCode;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table that is related to model
     *
     * @var string
     */
    protected $table = 'product';

    protected $primaryKey = 'PROD_ID';

    protected $appends = [
        'image',
        'category',
        'inventory'

    ];

    protected $hidden = [
        'PROD_IMG'
    ];

    protected function getCategoryAttribute()
    {
        $category = Category::where('CAT_ID', $this->attributes['CAT_ID'])->first();
        return $category;
    }


    protected function getInventoryAttribute()
    {
        $result = ProductCode::where('PROD_ID', $this->attributes['PROD_ID'])->first();
        return $result->inventory;
    }

    /**
     * Convert blob to base64
     *
     * @return string
     */
    protected function getImageAttribute()
    {
        return 'data:image/jpeg;base64,' . base64_encode($this->PROD_IMG);
    }

    public function transactions()
    {
        return $this->belongsToMany('App\Transaction', 'product_transaction', 'TRANSACTION_ID', 'PROD_ID')->withPivot('ORDERED_QUANTITY', 'CURRENT_PRICE');
    }

    public function feedbacks()
    {
        return $this->hasMany('App\Feedback', 'PROD_ID');
    }

    public function categories()
    {
        return $this->belongsTo('App\Category', 'CAT_ID');
    }

    public function codes()
    {
        return $this->belongsTo('App\ProductCode', 'PROD_ID');
    }
}
