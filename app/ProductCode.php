<?php

namespace App;

use App\Inventory;
use Illuminate\Database\Eloquent\Model;

class ProductCode extends Model
{

    protected $table = 'product_code';

    protected $primaryKey = 'PROD_CODE_ID';

    protected $hidden = [
        'PROD_CODE_NO',
        'PROD_CODE_IMG'
    ];

    protected $appends = [
        'inventory'
    ];

    public function getInventoryAttribute()
    {
        $inventory = Inventory::where('PROD_CODE_ID', $this->attributes['PROD_CODE_ID'])->first();
        return $inventory->INV_ONLINE_STOCK;
    }

    public function inventories()
    {
        return $this->hasOne('App\Inventory', 'PROD_CODE_ID');
    }

    public function products()
    {
        return $this->belongsTo('App\Product', 'PROD_ID');
    }
}
