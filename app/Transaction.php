<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Product;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $primaryKey = 'ID';

    protected $fillable = [
        'or_number',
        'total',
        'shipping_fee',
        'date_purchased',
        'payment_type',
        'CUST_ACCNT_ID',
        'shipper_address',
        'biller_name',
        'shipper_name',
        'biller_address',
        'contact_number',
        'email',
        'notes',
    ];

    protected $appends = [
        'date_added',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_transaction', 'TRANSACTION_ID', 'PROD_ID')->withPivot('ORDERED_QUANTITY', 'CURRENT_PRICE');
    }

    public function getDateAddedAttribute()
    {
        return Carbon::parse($this->attributes['CREATED_AT'])->diffForHumans();
    }

}
