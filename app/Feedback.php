<?php

namespace App;
use App\User;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';

    protected $fillable = [
        'RATE',
        'TITLE',
        'DESCRIPTION',
        'CUST_ACCNT_ID',
        'SERV_ID',
        'PACK_ID',
        'PROD_ID'
    ];

    protected $appends = [
        'reviewer',
        'human_date'
    ];

    public function getHumanDateAttribute()
    {
        return Carbon::parse($this->attribute['CREATED_AT'])->toDayDateTimeString();
    }

    public function getReviewerAttribute()
    {
        $user = User::where('CUST_ACCNT_ID', $this->attributes['CUST_ACCNT_ID'])->first();
        return $user->CUST_FNAME . ' ' . $user->CUST_LNAME;
    }

    public function customers()
    {
        return $this->belongsTo('App\User', 'CUST_ACCNT_ID');
    }

    public function products()
    {
        return $this->belongsTo('App\Product', 'PROD_ID');
    }

    public function services()
    {
        return $this->belongsTo('App\Service', 'SERV_ID');
    }

        public function packages()
    {
        return $this->belongsTo('App\Package', 'PACK_ID');
    }
}
