<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicy extends Model
{
    /**
     * The table that is related to model
     *
     * @var string
     */
    protected $table = 'privacy_policy';
}
