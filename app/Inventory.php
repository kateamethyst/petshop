<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventory';

    public function codes()
    {
        return belongsTo('App\ProductCode', 'PROD_CODE_ID');
    }
}
