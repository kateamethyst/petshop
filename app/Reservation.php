<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Reservation extends Model
{
    protected $table = 'reservations';

    protected $primaryKey = 'RESERVE_ID';

    protected $fillable = [
        'SERV_ID',
        'PACK_ID',
        'BRANCH_ID',
        'CUST_ACCNT_ID',
        'PAYMENT_TYPE',
        'AMOUNT',
        'START_DATE_RESERVED',
        'END_DATE_RESERVED',
        'NOTES',
        'OR_NUMBER',
        'PET_NAME',
        'PET_CATEG_ID',
        'AGE',
        'B_ID',
        'MARKINGCOLOR',
        'GENDER',
        'STATUS'
    ];

    protected $appends = [
        'date_added',
    ];

    public function services()
    {
        return $this->belongsTo('App\Service', 'SERV_ID');
    }

    public function branches()
    {
        return $this->belongsTo('App\Branch', 'BRANCH_ID');
    }

    public function packages()
    {
        return $this->belongsTo('App\Package', 'PACK_ID');
    }


    public function getDateAddedAttribute()
    {
        return Carbon::parse($this->attributes['CREATED_AT'])->diffForHumans();
    }

}
