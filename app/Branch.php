<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branch';

    protected $primaryKey = 'BRANCH_ID';

    public function reservations()
    {
        return $this->hasMany('App\Reservation', 'BRANCH_ID');
    }
}
