<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetCategory extends Model
{
    protected $table = 'pet_category';

    protected $primaryKey = 'PET_CATEG_ID';

    public function breed()
    {
        return $this->hasMany('App\Breed', 'PET_CATEG_ID');
    }
}
