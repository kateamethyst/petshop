<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'customer_account';

    protected $primaryKey = 'CUST_ACCNT_ID';

    protected $fillable = [
        'CUST_FNAME',
        'CUST_LNAME',
        'CUST_EMAIL',
        'CUST_USERNAME',
        'CUST_PASSWORD',
        'CUST_CONTACT',
    ];

    public $timestamps = false;

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->CUST_PASSWORD;
    }

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    public function getRememberTokenName()
    {
        return null; // not supported
    }

    /**
    * Overrides the method to ignore the remember token.
    */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute) {
            parent::setAttribute($key, $value);
        }
    }

    public function feedbacks()
    {
        return $this->hasMany('App\Feedback', 'CUST_ACCNT_ID');
    }
}
