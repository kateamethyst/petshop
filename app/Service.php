<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The table that is related to model
     *
     * @var string
     */
    protected $table = 'services';

    protected $primaryKey = 'SERV_ID';

    public $appends = [
        'range',
        'image',
        'time_duration'
    ];

    protected $hidden = [
        'SERV_IMG'
    ];

    public function getRangeAttribute()
    {
        $time = explode('.', $this->attributes['SERV_DURATION']);
        $minutes = 60*($time[1]/10)*.10;
        return $time[0] .' hour, ' . $minutes . ' mins';
    }

    public function getTimeDurationAttribute()
    {
        $time = explode('.', $this->attributes['SERV_DURATION']);
        $minutes = 60*($time[1]/10)*.10;
        return $time[0] .':' . $minutes;
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation', 'SERV_ID');
    }

    /**
     * Convert blob to base64
     *
     * @return string
     */
    public function getImageAttribute()
    {
        return 'data:image/jpeg;base64,' . base64_encode($this->SERV_IMG);
    }

    public function feedbacks()
    {
        return $this->hasMany('App\Feedback', 'SERV_ID');
    }
}
