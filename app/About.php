<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    /**
     * The table that is related to model
     *
     * @var string
     */
    protected $table = 'about_us';
}
