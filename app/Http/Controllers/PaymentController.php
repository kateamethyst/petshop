<?php

namespace App\Http\Controllers;

use Stripe\Product as StoreStripe;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ShippingAddress;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;

use DB;
use App\Transaction as TransactionModel;
use App\Mail\InvoiceEmail;
use Mail;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $apiContext;

    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $paypal_conf = \Config::get('paypal');
        $this->apiContext = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->apiContext->setConfig($paypal_conf['settings']);
    }

    /**
     * Pay checkout
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function payWithStripe(Request $request)
    {

        $customer = Customer::create([
            'email' => $request->stripeEmail,
            'source' => $request->stripeToken
        ]);

        $charge = Charge::create(array(
            'customer' => $customer->id,
            'amount'   => floatval($request->amount) * 100,
            'currency' => 'PHP'
        ));

        $products = json_decode($request->products, true);
        $user = json_decode($request->user, true);
        $biller = $user['biller'];
        $shipper = $user['shipper'];

        $transaction = TransactionModel::create([
            'total'            => $request->amount,
            'date_purchased'   => date('Y-m-d H:i:s'),
            'shipping_fee'     => $request->rate,
            'payment_type'     => 'stripe',
            'email'            => $biller['email'],
            'contact_number'   => $biller['phone'],
            'biller_name'      => $biller['name'],
            'biller_address'   => $biller['address'] . ' ' . $biller['city'] . ' ' . $biller['province'] . ' ' .  $biller['postal'] . ' PH' ,
            'shipper_name'     => $shipper['name'],
            'shipper_address'  => $shipper['address'] . ' ' . $shipper['city'] . ' ' . $shipper['province'] . ' ' .  $shipper['postal'] . ' PH' ,
            'CUST_ACCNT_ID'    => auth()->user()->CUST_ACCNT_ID,
            'status'           => 'In Progress'
        ]);

        foreach ($products as $key => $product) {
            $items[] = (object) [
                'name'     => $product['PROD_NAME'],
                'quantity' => $product['quantity'],
                'image'    => $product['PROD_IMG'],
                'price'    => $product['PROD_PRICE']
            ];

            $result = DB::select('SELECT * from product_code where PROD_ID = :id', ['id' => $product['PROD_ID']]);

            $prodCodes = $result[0];

            DB::table('inventory')->where('PROD_CODE_ID', $prodCodes->PROD_CODE_ID)->update(['INV_ONLINE_STOCK' => $product['inventory'] - $product['quantity']]);

            $transaction->products()->attach($product['PROD_ID'], ['ordered_quantity' => $product['quantity'], 'current_price' => $product['PROD_PRICE']]);
        }

        Mail::to($biller['email'])->send(new InvoiceEmail([
            'id'              => $transaction->ID,
            'date_purchased'  => date('Y-m-d H:i:s'),
            'products'        => $items,
            'total'           => $request->amount,
            'biller_address'  => $biller['address'] . ' ' . $biller['city'] . ' ' . $biller['province'] . ' ' .  $biller['postal'] . ' PH',
            'shipper_address' => $shipper['address'] . ' ' . $shipper['city'] . ' ' . $shipper['province'] . ' ' .  $shipper['postal'] . ' PH',
            'biller_name'     => $biller['name'],
            'shipper_name'    => $shipper['name'],
            'rate'            => $request->rate
        ]));

        return redirect()->route('thanks');
    }

    /**
     * [payWithpaypal description]
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function payWithpaypal(Request $request)
    {
        session(['user' => $request->user['biller']['email']]);
        session(['information' => [
            'user' => $request->user,
            'rate' => $request->shipping,
            'amount' => $request->amount,
            'items' => $request->items
        ]]);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $productList = new ItemList();

        foreach ($request->items as $key => $item) {
            $product =  new Item();
            $cartItem[] = $product->setName($item['PROD_NAME'])
                ->setCurrency('PHP')
                ->setQuantity($item['quantity'])
                ->setPrice($item['PROD_PRICE']);
        }

        $productList->setItems($cartItem);

        $details = new Details();
        $details->setSubtotal($request->subtotal)
                ->setTax(0)
                ->setShipping($request->shipping);

        $amount = new Amount();
        $amount->setCurrency('PHP')
                    ->setTotal($request->amount)
                    ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setItemList($productList)
                    ->setAmount($amount);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('process-paypal'))->setCancelUrl(URL::route('process-paypal'));

        $payment = new Payment();
        $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));

        try {
            $paid = $payment->create($this->apiContext);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            return response()->json($redirect_url);
        }

        \Session::put('error', 'Unknown error occurred');
                return Redirect::route('thankssss');
    }

    /**
     * [processPaypal description]
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function processPaypal(Request $request)
    {
        $payment_id = $request->paymentId;
        $payment = Payment::get($payment_id, $this->apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->PayerID);
        try {
            $result = $payment->execute($execution, $this->apiContext);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        }

        if ($result->getState() == 'approved') {
            $transactions = $payment->getTransactions();
            $details = $transactions[0]->getItemList();
            $amount = $transactions[0]->getAmount();
            $shipping_rate = $amount->getDetails();

            $information = session('information');
            $items = $information['items'];
            $user = $information['user'];
            $biller = $user['biller'];
            $shipper = $user['shipper'];

            $transaction = TransactionModel::create([
                'total'            => $amount->total,
                'date_purchased'   => date('Y-m-d H:i:s'),
                'shipping_fee'     => $shipping_rate->shipping,
                'payment_type'     => 'paypal',
                'email'            => $biller['email'],
                'contact_number'   => $biller ['phone'],
                'biller_name'      => $biller['name'],
                'biller_address'   => $biller['address'] . ' ' . $biller['city'] . ' ' . $biller['province'] . ' ' .  $biller['postal'] . ' PH' ,
                'shipper_name'     => $shipper['name'],
                'shipper_address'  => $shipper['address'] . ' ' . $shipper['city'] . ' ' . $shipper['province'] . ' ' .  $shipper['postal'] . ' PH' ,
                'CUST_ACCNT_ID'      => auth()->user()->CUST_ACCNT_ID,
                'status'           => 'In Progress'
            ]);

            Mail::to(session('user'))->send(new InvoiceEmail([
                'id'               => $transaction->ID,
                'products'         => $details->items,
                'date_purchased'   => date('Y-m-d H:i:s'),
                'biller_address'  => $biller['address'] . ' ' . $biller['city'] . ' ' . $biller['province'] . ' ' .  $biller['postal'] . ' PH',
                'shipper_address' => $shipper['address'] . ' ' . $shipper['city'] . ' ' . $shipper['province'] . ' ' .  $shipper['postal'] . ' PH',
                'biller_name'     => $biller['name'],
                'shipper_name'    => $shipper['name'],
                'total'            => $amount->total,
                'rate'             => $shipping_rate->shipping
            ]));

            foreach ($items as $key => $product) {
                $result = DB::select('SELECT * from product_code where PROD_ID = :id', ['id' => $product['PROD_ID']]);

                $prodCodes = $result[0];

                DB::table('inventory')->where('PROD_CODE_ID', $prodCodes->PROD_CODE_ID)->update(['INV_ONLINE_STOCK' => $product['inventory'] - $product['quantity']]);


                $transaction->products()->attach($product['PROD_ID'], ['ordered_quantity' => $product['quantity'], 'current_price' => $product['PROD_PRICE']]);
            }

            return Redirect::route('thanks');
        }
    }
}
