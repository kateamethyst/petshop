<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Service $model)
    {
        $services = $model->where('SERV_STATUS', 'Available')->get();
        return response()->json([
            'code'    => 200,
            'message' => 'Success',
            'data'    => $services
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Service $model, $id)
    {
        $services = $model->where('SERV_ID', $id)->first();
        return response()->json([
            'code'    => 200,
            'message' => 'Success',
            'data'    => $services
        ], 200);
    }
}
