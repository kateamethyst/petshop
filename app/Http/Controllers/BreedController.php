<?php

namespace App\Http\Controllers;

use App\Breed;
use Illuminate\Http\Request;

class BreedController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breeds = Breed::where('PET_CATEG_ID', $id)->where('B_STATUS','Active')->get();
        return response()->json($breeds, 200);
    }
}
