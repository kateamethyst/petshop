<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Product as StoreStripe;
use Stripe\Stripe;
use App\Product;

class ProductController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $model)
    {
        $products = $model->where('PROD_STATUS', 'Available')->get();
        return response()->json([
            'code'    => 200,
            'message' => 'Success',
            'data'    => $products
        ], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $model, $id)
    {
        $products = $model->where('PROD_ID', $id)->first();
        return response()->json([
            'code'    => 200,
            'message' => 'Success',
            'data'    => $products
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkoutProducts(Request $request)
    {
        $plan = StoreStripe::create([
            'name' => $request->name,
            'type' => 'good',
            'shippable' => true,
            'metadata' => [
                'product_id' => $request->id
            ]
        ]);

        $product = StoreStripe::retrieve($plan->id);

        return response()->json([
            'code'    => 200,
            'message' => 'Success',
            'data'    => $product
        ], 200);
    }
}
