<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\WelcomeEmail;
use App\Mail\ContactEmail;
use App\Mail\ForgotPasswordEmail;
use Mail;
use Illuminate\Support\Facades\Hash;
use DB;
use App\User;

class UserController extends Controller
{

    public function sendMessage(Request $request)
    {
        Mail::to('uteresjeffreygarcia@gmail.com')->send(new ContactEmail([
            'email'   => $request->email,
            'name'    => $request->name,
            'message_request' => $request->message
        ]));

        return redirect('contact')->with('message', 'Successfully sent, please wait to our reply. Thanks');
    }

    /**
     * [registration description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function registration(Request $request)
    {
        $password = str_random(5);

        $find = DB::table('customer_account')->where('CUST_EMAIL', $request->email)->orWhere('CUST_USERNAME', $request->username)->get();
        if(count($find) > 0) {
            return redirect('register')->withInput()->with('messageError', 'Email or username was already registered.');
        }

        $params = [
            'CUST_FNAME' => $request->first_name,
            'CUST_LNAME' => $request->last_name,
            'CUST_EMAIL' => $request->email,
            'CUST_USERNAME' => $request->username,
            'CUST_PASSWORD' => bcrypt($password),
            'CUST_CONTACT' => $request->phone,
            'CUST_CREATE_DATE' => date('Y-m-d H:i:s')
        ];

        $user = DB::table('customer_account')->insert($params);

        Mail::to($request->email)->send(new WelcomeEmail([
            'password' => $password,
            'name' => $request->first_name . ' '  . $request->last_name,
        ]));
        return redirect('register')->with('message', 'Successfully registered. Please check you email to login.');
    }

    public function updateProfile(Request $request)
    {
        $params = [
            'CUST_FNAME'    => $request->CUST_FNAME,
            'CUST_MI'       => $request->CUST_MI,
            'CUST_LNAME'    => $request->CUST_LNAME,
            'CUST_EMAIL'    => $request->CUST_EMAIL,
            'CUST_USERNAME' => $request->CUST_USERNAME,
            'CUST_CONTACT'  => $request->CUST_CONTACT,
        ];

        $user = User::where('CUST_ACCNT_ID', auth()->user()->CUST_ACCNT_ID)->update($params);

        return redirect('/customer/account')->with('message', 'Successfully updated your profile.');
    }

    public function updatePassword(Request $request)
    {
        $result = User::where('CUST_ACCNT_ID', auth()->user()->CUST_ACCNT_ID)->get();
        $users = $result[0];
        if (Hash::check($request->old_password, $users->CUST_PASSWORD)) {
            $password = bcrypt($request->new_password);
            $users = User::where('CUST_ACCNT_ID', auth()->user()->CUST_ACCNT_ID)->update(['CUST_PASSWORD' => $password]);
            return redirect('/customer/account')->with('password', 'Successfully updated your password.');
        }

        return redirect('/customer/account')->with('passwordError', 'Please double check old password.');
    }

    public function forgotPassword(Request $request)
    {
        $result = User::where('CUST_EMAIL', $request->email)->get();
        if (!isset($result[0]) || empty($result[0])) {
            return redirect('/forgot')->with('error', 'Email was not registered.');
        }

        $user = $result[0];

        if ($user) {
            Mail::to($request->email)->send(new ForgotPasswordEmail([
                'id' => $user->CUST_ACCNT_ID
            ]));

            return redirect('/forgot')->with('message', 'An email with password reset instructions has been sent to your email address, if it exists on our system.');
        }

        return redirect('/forgot')->with('error', 'Email was not registered.');

    }

    public function resetPassword(Request $request)
    {
        $result = User::where('CUST_ACCNT_ID', $request->id)->get();
        $user = $result[0];

        if ($user) {
            $password = bcrypt($request->password);
            $users = User::where('CUST_ACCNT_ID',  $request->id)->update(['CUST_PASSWORD' => $password]);
            return redirect('/login')->with('message', 'Successfully reset password. You can now login using your new password.');
        }

        return redirect('/forgot')->with('error', 'Error, something might happen');

    }
}
