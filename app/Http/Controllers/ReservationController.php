<?php

namespace App\Http\Controllers;


use Stripe\Product as StoreStripe;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Auth;
use DB;
use Illuminate\Http\Request;
use PDF;

use App\User;
use App\Reservation;
use App\Service;
use App\Package;
use App\Breed;
use App\PetCategory;
use App\Branch;
use App\Setting;

use Carbon\Carbon;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\ShippingAddress;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use Yajra\Datatables\Datatables;
use App\Mail\ReservationEmail;
use Mail;


class ReservationController extends Controller
{
    protected $apiContext;

    public function ajax()
    {
        $reservations = Reservation::query()->where('CUST_ACCNT_ID', auth()->user()->CUST_ACCNT_ID);
        return Datatables::of($reservations)
                ->addColumn('action', function ($reservation) {
                    return '<a href="' . route('customer.view.reservation', ['id' => $reservation->RESERVE_ID ]) . '" class="btn btn-xs btn-primary">View</a>';
                })
                ->make(true);
    }

    public function viewReservation(Request $request, $id)
    {
        return view('customer.reserve_details');
    }

    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $paypal_conf = \Config::get('paypal');
        $this->apiContext = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->apiContext->setConfig($paypal_conf['settings']);
    }

    public function checkAvailability(Request $request)
    {
        $reservation = Reservation::with('services')->where('BRANCH_ID', $request->branch_id)->where('SERV_ID', $request->serv_id)->where('date_reserved', $request->date_reserved)->get();

        return response()->json($reservation, 200);

    }

    public function isReserve(Request $request, Reservation $model)
    {

        $item = json_decode($request->item, true);
        $serviceDuration = explode(':', $item['time_duration']);

        $requestedBookDate = date_format(date_create($request->date_reserved), 'Y-m-d');
        $requestedBookTime =  date('H:i:s', strtotime($request->time_reserved));
        $requestedBookTimeEnd =  Carbon::parse($requestedBookTime)->addHours($serviceDuration['0'])->addMinutes($serviceDuration['1'])->toTimeString();

        $reservations = $model->whereDate('START_DATE_RESERVED', $requestedBookDate)->where('BRANCH_ID', $request->branch_id)->get();

        if (!$reservations->isEmpty()) {

            foreach ($reservations as $key => $reservation) {
                $bookedStartTime = date('H:i:s', strtotime($reservation['START_DATE_RESERVED']));
                $bookedEndTime = date('H:i:s', strtotime($reservation['END_DATE_RESERVED']));

                // Check if the start time is between start time and end time

                if ($requestedBookTime >= $bookedStartTime && $requestedBookTime <= $bookedEndTime) {
                    return response()->json(false, 200);
                }

                if ($requestedBookTimeEnd >= $bookedStartTime && $requestedBookTimeEnd <= $bookedEndTime) {
                    return response()->json(false, 200);
                }
            }
        }

        return response()->json(true, 200);

    }

    public function payWithStripe(Request $request)
    {
        $result = Setting::first();

        $customer = Customer::create([
            'email' => $request->stripeEmail,
            'source' => $request->stripeToken
        ]);

        $charge = Charge::create(array(
            'customer' => $customer->id,
            'amount'   => floatval($result->RESERVE_FEE) * 100,
            'currency' => 'PHP'
        ));

        $requestDateReservation = date_format(date_create($request->date_reserved), 'Y-m-d') . ' '  . date('H:i:s', strtotime($request->time_reserved));
        $requestStartTime = date('H:i:s', strtotime($request->time_reserved));
        $petInformation = json_decode($request->pet_information, true);

        $params = [
            'BRANCH_ID'           => $request->branch_id,
            'CUST_ACCNT_ID'       => auth()->user()->CUST_ACCNT_ID,
            'PAYMENT_TYPE'        => 'stripe',
            'AMOUNT'              => $result->RESERVE_FEE,
            'START_DATE_RESERVED' => Carbon::parse($requestDateReservation)->toDateTimeString(),
            'PET_NAME'            => $petInformation['name'],
            'PET_CATEG_ID'        => $petInformation['pet_category_id'],
            'MARKINGCOLOR'        => $petInformation['markingcolor'],
            'GENDER'              => $petInformation['gender'],
            'B_ID'                => $petInformation['breed'],
            'AGE'                 => $petInformation['age']
        ];

        if ($request->has('serv_id')) {
            $item = Service::where('SERV_ID', intval($request->serv_id))->first();
            $params['SERV_ID'] = $request->serv_id;
            if (isset($item) && !empty($item)) {
                $addTime = explode(':', $item->time_duration);
                $params['END_DATE_RESERVED'] =  date_format(date_create($request->date_reserved), 'Y-m-d') . ' '  . Carbon::parse($requestStartTime)->addHours($addTime['0'])->addMinutes($addTime['1'])->toTimeString();
            }
        }

        if ($request->has('pack_id')) {
            $item = Package::where('pack_id', intval($request->pack_id))->first();
            $params['PACK_ID'] = $request->pack_id;
            if (isset($item) && !empty($item)) {
                $addTime = explode(':', $item->time_duration);
                $params['END_DATE_RESERVED'] =  date_format(date_create($request->date_reserved), 'Y-m-d') . ' '  . Carbon::parse($requestStartTime)->addHours($addTime['0'])->addMinutes($addTime['1'])->toTimeString();
            }
        }

        $reservation = Reservation::create($params);

        $reserveDetails = json_decode($request->item_info, true);
        $itemDetails = json_decode($reserveDetails['item']);
        $branch = Branch::where('BRANCH_ID', $request->branch_id)->first();

        $pet_categories = PetCategory::where('PET_CATEG_ID', $petInformation['pet_category_id'])->first();
        $breeds = Breed::where('B_ID', $petInformation['breed'])->first();

        if ($reservation) {
            Mail::to(auth()->user()->CUST_EMAIL)->send(new ReservationEmail([
                'id'            => $reservation->RESERVE_ID,
                'name'          => $itemDetails->name,
                'date'          => date('F d, Y H:i a', strtotime( $reserveDetails['reserve_date'])),
                'branch'        => $branch['BRANCH_NAME'] . ' - ' . $branch['BRANCH_ADDRESS'],
                'details'       => $itemDetails->details,
                'customer_name' => auth()->user()->CUST_FNAME . ' ' . auth()->user()->CUST_LNAME,
                'age'           => $petInformation['age'],
                'pet_name'      => $petInformation['name'],
                'gender'        => $petInformation['gender'],
                'color'         => $petInformation['markingcolor'],
                'pet_category'  => $pet_categories->PET_CATEG_NAME,
                'breed'         => $breeds->B_NAME
            ]));
        }

        return redirect()->route('thanks');
    }

    public function payWithPaypal(Request $request)
    {
        $result = Setting::first();
        $reservation_fee = $result->RESERVE_FEE;
        session(['information' => $request->reservation]);
        session(['pet' => $request->pet]);
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item =  new Item();
        $item->setName($request->name)->setCurrency('PHP')->setQuantity(1)->setPrice($reservation_fee);

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $details = new Details();
        $details->setSubtotal($reservation_fee)->setTax(0);

        $amount = new Amount();
        $amount->setCurrency('PHP')->setTotal($reservation_fee)->setDetails($details);

        $transaction = new Transaction();
        $transaction->setItemList($itemList)->setAmount($amount)->setDescription("Pay reservation fee from PayPal " . $request->name)
            ->setInvoiceNumber(uniqid());

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('process-reservation-paypal'))->setCancelUrl(URL::route('reserve'));

        $payment = new Payment();
        $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));

        try {
            $paid = $payment->create($this->apiContext);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            return response()->json($redirect_url);
        }

        \Session::put('error', 'Unknown error occurred');
                return Redirect::route('index');
    }

    public function processPaypal(Request $request)
    {
        $result = Setting::first();
        $reservation_fee = $result->RESERVE_FEE;

        $payment_id = $request->paymentId;
        $payment = Payment::get($payment_id, $this->apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->PayerID);

        try {
            // $result = $payment->execute($execution, $this->apiContext);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        }

        $information = json_decode(session('information'), true);
        $petInformation = json_decode(session('pet'), true);
        $item = json_decode($information['item'], true);

        // dd($information);
        $requestDateReservation = date_format(date_create($information['date_reserved']), 'Y-m-d') . ' '  . date('H:i:s', strtotime($information['time_reserved']));
        $requestStartTime = date('H:i:s', strtotime($information['time_reserved']));
        $params = [
            'BRANCH_ID'           => $information['branch_id'],
            'CUST_ACCNT_ID'       => auth()->user()->CUST_ACCNT_ID,
            'PAYMENT_TYPE'        => 'paypal',
            'AMOUNT'              => $reservation_fee,
            'START_DATE_RESERVED' => Carbon::parse($requestDateReservation)->toDateTimeString(),
            'PET_NAME'            => $petInformation['name'],
            'PET_CATEG_ID'        => $petInformation['pet_category_id'],
            'MARKINGCOLOR'        => $petInformation['markingcolor'],
            'GENDER'              => $petInformation['gender'],
            'B_ID'                => $petInformation['breed'],
            'AGE'                 => $petInformation['age']
        ];

        if ($information['type'] == 'service') {
            $params['SERV_ID'] = $information['book_id'];
            $result = Service::where('SERV_ID', intval($information['book_id']))->first();
            if (isset($result) && !empty($result)) {
                $addTime = explode(':', $item['time_duration']);
                $params['END_DATE_RESERVED'] =  date_format(date_create($information['date_reserved']), 'Y-m-d') . ' '  . Carbon::parse($requestStartTime)->addHours($addTime['0'])->addMinutes($addTime['1'])->toTimeString();
            }
        } else {
            $params['PACK_ID'] = $information['book_id'];
            $result = Package::where('pack_id', intval($information['book_id']))->first();
            if (isset($result) && !empty($result)) {
                $addTime = explode(':', $item['time_duration']);
                $params['END_DATE_RESERVED'] =  date_format(date_create($information['date_reserved']), 'Y-m-d') . ' '  . Carbon::parse($requestStartTime)->addHours($addTime['0'])->addMinutes($addTime['1'])->toTimeString();
            }
        }

        $reservation = Reservation::create($params);

        $branch = Branch::where('BRANCH_ID', $information['branch_id'])->first();

        $pet_categories = PetCategory::where('PET_CATEG_ID', $petInformation['pet_category_id'])->first();
        $breeds = Breed::where('B_ID', $petInformation['breed'])->first();

        if ($reservation) {
            Mail::to(auth()->user()->CUST_EMAIL)->send(new ReservationEmail([
                'id'            => $reservation->RESERVE_ID,
                'name'          => $item['name'],
                'branch'        => $branch['BRANCH_NAME'] . ' - ' . $branch['BRANCH_ADDRESS'],
                'date'          => date('F d, Y H:i a', strtotime($information['reserve_date'])),
                'details'       => $item['details'],
                'customer_name' => auth()->user()->CUST_FNAME . ' ' . auth()->user()->CUST_LNAME,
                'age'           => $petInformation['age'],
                'pet_name'      => $petInformation['name'],
                'gender'        => $petInformation['gender'],
                'color'         => $petInformation['markingcolor'],
                'pet_category'  => $pet_categories->PET_CATEG_NAME,
                'breed'         => $breeds->B_NAME
            ]));
        }

        return redirect()->route('thanks');
    }

    /**
     * [viewReservationEmail description]
     *
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function viewReservationEmail($id)
    {
        $query = Reservation::where('RESERVE_ID', $id)->with('branches');

        $reservation = Reservation::where('RESERVE_ID', $id)->first();

        if (!$reservation) {
            return redirect('/');
        }

        if ($reservation['SERV_ID'] != '') {
            $query = $query->with('services');
        }

        if ($reservation['PACK_ID'] != '') {
            $query = $query->with('packages');
        }

        $reservations = $query->get();

        $id = $reservations[0]['RESERVE_ID'];
        $branch = $reservations[0]['branches']['BRANCH_NAME'] . ' - ' . $reservations[0]['branches']['BRANCH_ADDRESS'];
        $date = date('F d, Y H:i a', strtotime($reservations[0]['START_DATE_RESERVED']));

        if ($reservation['SERV_ID'] != '') {
            $name = $reservations[0]['services']['SERV_NAME'];
            $details = $reservations[0]['services']['SERV_DETAILS'];
        }

        if ($reservation['PACK_ID'] != '') {
            $name = $reservations[0]['packages']['PACK_NAME'];
            $details = $reservations[0]['packages']['PACK_DETAILS'];
        }

        $pet_categories = PetCategory::where('PET_CATEG_ID', $reservations[0]['PET_CATEG_ID'])->first();
        $breeds = Breed::where('B_ID', $reservations[0]['B_ID'])->first();
        $user = User::where('CUST_ACCNT_ID', $reservations[0]['CUST_ACCNT_ID'])->first();

        $customer_name = $user->CUST_FNAME . ' ' . $user->CUST_LNAME;
        $pet_name      = $reservations[0]['PET_NAME'];
        $pet_category  = $pet_categories->PET_CATEG_NAME;
        $breed         = $breeds->B_NAME;
        $age           = $reservations[0]['AGE'];
        $gender        = $reservations[0]['GENDER'];
        $color         = $reservations[0]['MARKINGCOLOR'];

        return view('mail.reservation', compact('id', 'branch', 'details', 'name', 'date', 'customer_name', 'pet_name', 'pet_category', 'breed', 'age', 'gender', 'color'));
    }

    public function pdf($id)
    {
        $query = Reservation::where('RESERVE_ID', $id)->with('branches');

        $reservation = Reservation::where('RESERVE_ID', $id)->first();

        if ($reservation['SERV_ID'] != '') {
            $query = $query->with('services');
        }

        if ($reservation['PACK_ID'] != '') {
            $query = $query->with('packages');
        }

        $reservations = $query->get();

        $id = $reservations[0]['RESERVE_ID'];
        $branch = $reservations[0]['branches']['BRANCH_NAME'] . ' - ' . $reservations[0]['branches']['BRANCH_ADDRESS'];
        $date = date('F d, Y H:i a', strtotime($reservations[0]['START_DATE_RESERVED']));

        if ($reservation['SERV_ID'] != '') {
            $name = $reservations[0]['services']['SERV_NAME'];
            $details = $reservations[0]['services']['SERV_DETAILS'];
        }

        if ($reservation['PACK_ID'] != '') {
            $name = $reservations[0]['packages']['PACK_NAME'];
            $details = $reservations[0]['packages']['PACK_DETAILS'];
        }

        $user = User::where('CUST_ACCNT_ID', $reservations[0]['CUST_ACCNT_ID'])->first();
        $pet_categories = PetCategory::where('PET_CATEG_ID', $reservations[0]['PET_CATEG_ID'])->first();
        $breeds = Breed::where('B_ID', $reservations[0]['B_ID'])->first();

        $pdf = PDF::loadView('mail.reservation', [
            'id'            => $id,
            'name'          => $name,
            'details'       => $details,
            'branch'        => $branch,
            'date'          => $date,
            'customer_name' => $user->CUST_FNAME . ' ' . $user->CUST_LNAME,
            'pet_name'      => $reservations[0]['PET_NAME'],
            'pet_category'  => $pet_categories->PET_CATEG_NAME,
            'breed'         => $breeds->B_NAME,
            'age'           => $reservations[0]['AGE'],
            'gender'        => $reservations[0]['GENDER'],
            'color'         => $reservations[0]['MARKINGCOLOR']
        ]);

        $fileName = 'Reservation'.date('Y-m-d') . '.pdf';
        return $pdf->download($fileName);
    }
}
