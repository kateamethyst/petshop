<?php

namespace App\Http\Controllers;

use Auth;
use Yajra\Datatables\Datatables;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Admin Page
     *'{!! secure_url(URL::route('transaction', [], false)); !!}',
     * @return [type] [description]
     */
    public function ajax()
    {
        $transactions = Transaction::query()->where('CUST_ACCNT_ID', auth()->user()->CUST_ACCNT_ID);
        return Datatables::of($transactions)
                ->addColumn('action', function ($transaction) {
                    return '<a href="' . route('customer.view.transaction', ['id' => $transaction->ID ]) . '" class="btn btn-xs btn-primary">View</a>';
                })
                ->make(true);
    }
}
