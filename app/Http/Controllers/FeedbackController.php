<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Feedback;

class FeedbackController extends Controller
{
    public function saveFeedback(Request $request)
    {
        $params = [
            'RATE'          => 0,
            'DESCRIPTION'   => $request->description,
            'TITLE'         => $request->title,
            'CUST_ACCNT_ID' => auth()->user()->CUST_ACCNT_ID,
        ];

        if ($request->has('product_id')) {
            $params['PROD_ID'] = $request->product_id;
        }

        if ($request->has('serv_id')) {
            $params['SERV_ID'] = $request->serv_id;
        }

        if ($request->has('pack_id')) {
            $params['PACK_ID'] = $request->pack_id;
        }

        $feedback = Feedback::create($params);

        return redirect()->back();
    }
}
