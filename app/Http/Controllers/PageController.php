<?php

namespace App\Http\Controllers;

use App\About;
use App\PrivacyPolicy;
use App\Product;
use App\Service;
use App\Feedback;
use App\Category;
use App\Package;
use App\Branch;
use App\Breed;
use App\PetCategory;
use App\Setting;

use DB;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about(About $model)
    {
        $abouts = $model->all();
        return view('about', compact('abouts'));
    }

    protected function _createTimeRange($start, $end, $interval = '30 mins', $format = '12')
    {
        $startTime = strtotime($start);
        $endTime   = strtotime($end);
        $returnTimeFormat = ($format == '12')?'g:i:s A':'G:i:s';

        $current   = time();
        $addTime   = strtotime('+'.$interval, $current);
        $diff      = $addTime - $current;

        $times = array();
        while ($startTime < $endTime) {
            $times[] = date($returnTimeFormat, $startTime);
            $startTime += $diff;
        }
        $times[] = date($returnTimeFormat, $startTime);
        return $times;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function privacyPolicy(PrivacyPolicy $model)
    {
        $privacies = $model->all();
        return view('privacy-policy', compact('privacies'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Product $product, Service $service, Package $package, Category $category)
    {
        $products = $product->where('PROD_STATUS', 'Available')->limit(10)->get();
        $services = $service->where('SERV_STATUS', 'Available')->limit(3)->get();
        $packages = $package->where('PACK_STATUS', 'Available')->limit(3)->get();
        $categories = $category->where('CAT_STATUS', 'Active')->limit(6)->get();
        return view('index', compact('products', 'services', 'packages', 'categories'));
    }

    public function services(Category $category, Service $service)
    {
        $services = $service->where('SERV_STATUS', 'Available')->paginate(15);
        $count = count($services);
        $categories = $category->where('CAT_STATUS', 'Active')->limit(6)->get();
        return view('services', compact('services', 'count', 'categories'));
    }

    public function packages(Category $category, Package $package)
    {
        $packages = $package->where('PACK_STATUS', 'Available')->paginate(15);
        $count = count($packages);
        $categories = $category->where('CAT_STATUS', 'Active')->limit(6)->get();
        return view('packages', compact('packages', 'count', 'categories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function result(Product $product, Category $category, Request $request)
    {
        $product = $product->where('PROD_STATUS', 'Available');
        $categoryName = 'All Products';
        if ($request->has('category')) {
            $product = $product->where('CAT_ID', $request->category);
            $result = $category->where('CAT_ID', $request->category)->first();
            $categoryName = $result->CAT_NAME;
        }

        if ($request->has('q')) {
            $product = $product->where('PROD_NAME', 'like', '%'.$request->q . '%');
            $categoryName = $request->q;
        }

        $products = $product->paginate(15);

        $count = count($products);

        $categories = $category->where('CAT_STATUS', 'Active')->get();
        return view('result', compact('products', 'categories', 'categoryName', 'count'));
    }


    public function getProduct(Product $model, $id)
    {
        $product = $model->where('PROD_ID', $id)->where('PROD_STATUS', 'Available')->with('feedbacks')->first();

        if (empty($product)) {
            return abort(404);
        }

        $products = $model->where('PROD_STATUS', 'Available')->inRandomOrder()->limit(5)->get();
        return view('product', compact('product', 'products'));
    }

    public function getService(Service $model, $id)
    {
        $service = $model->where('SERV_ID', $id)->where('SERV_STATUS', 'Available')->with('feedbacks')->first();

        if (empty($service)) {
            return abort(404);
        }

        $range = $this->_createTimeRange('9:00', '17:00', $service->range);
        $services = $model->where('SERV_STATUS', 'Available')->inRandomOrder()->limit(5)->get();
        $branches = Branch::where('BRANCH_STATUS', 'Active')->get();
        return view('book-service', compact('service', 'services', 'branches', 'range'));
    }

    public function getPackage(Package $model, $id)
    {
        $package = $model->where('PACK_ID', $id)->where('PACK_STATUS', 'Available')->with('feedbacks')->first();

        if (empty($package)) {
            return abort(404);
        }

        $range = $this->_createTimeRange('9:00', '17:00', $package->range);
        $packages = $model->where('PACK_STATUS', 'Available')->inRandomOrder()->limit(5)->get();
        $branches = Branch::where('BRANCH_STATUS', 'Active')->get();
        return view('book-package', compact('package', 'packages', 'branches', 'range'));
    }

    public function checkout()
    {
        $provinceShipping = DB::select("SELECT p.PROV_NAME as 'province', s.SHIPP_RATE_VALUE as 'shipping_rate' FROM shipping_rate as s , province as p where p.PROV_ID = s.PROV_ID");

        return view('checkout', compact('provinceShipping'));
    }

    public function billing()
    {
        return view('billing');
    }

    public function register()
    {
        return view('register');
    }

    public function getCities(Request $request)
    {
        $cities = DB::select("SELECT c.CITY_NAME as 'name', c.CITY_ID FROM city as c, province as p WHERE p.PROV_ID = c.PROV_ID and p.PROV_NAME = ?", [$request->province]);

        return response()->json($cities, 200);
    }


    public function reserve()
    {
        $first_categ = PetCategory::where('PET_CATEG_STATUS', 'Active')->first();
        $breeds = Breed::where('B_STATUS', 'Active')->where('PET_CATEG_ID', $first_categ->PET_CATEG_ID)->get();

        $pet_categories = PetCategory::where('PET_CATEG_STATUS', 'Active')->get();
        $result = Setting::first();
        $reservation_fee = $result->RESERVE_FEE;
        return view('reserve', compact('breeds', 'pet_categories', 'reservation_fee'));
    }

}
