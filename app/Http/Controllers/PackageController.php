<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Package $model)
    {
        $packages = $model->where('PACK_STATUS', 'Available')->get();
        return response()->json([
            'code'    => 200,
            'message' => 'Success',
            'data'    => $packages
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Package $model, $id)
    {
        $packages = $model->where('PACK_ID', $id)->first();
        return response()->json([
            'code'    => 200,
            'message' => 'Success',
            'data'    => $packages
        ], 200);
    }
}
