<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    /**
     * Returns the login view
     * @return View
     */
    public function getLogin()
    {
        return view('login');
    }

    /**
     * Clears the current user session
     * Redirect back to the landing page
     * @return Redirect
     */
    public function logout()
    {
        Auth::logout();
        return redirect(route('index'));
    }

    /**
     * An api logout endpoint
     *
     * @return Response
     */
    public function apiLogout()
    {
        Auth::logout();
        return $this->respond();
    }

    /**
     * Allows the Illuminate\AuthenticateUsers to accept email
     *
     * @param  Request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'CUST_USERNAME';

        return [
            $field => $request->get($this->username()),
            'password' => $request->CUST_PASSWORD,
        ];
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'CUST_PASSWORD' => 'required|string',
        ]);
    }

    public function username()
    {
        return 'CUST_EMAIL';
    }

    /**
     * Determine if the user has too many failed login attempts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), 3, 15
        );
    }

}
