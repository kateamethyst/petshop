<?php

namespace App\Http\Controllers;

use Auth;
use App\Transaction;
use Illuminate\Http\Request;
use App\Reservation;
use PDF;

class AdminController extends Controller
{
    /**
     * Admin Page
     *
     * @return [type] [description]
     */
    public function index()
    {
        return view('customer.index');
    }

    /**
     * Account Page
     *
     * @return [type] [description]
     */
    public function account()
    {
        $user = Auth::user();
        return view('customer.account', compact('user'));
    }

    public function viewTransaction(Request $request, $id)
    {
        $result = Transaction::with('products')->where('CUST_ACCNT_ID', auth()->user()->CUST_ACCNT_ID)->where('ID', $id)->get();
        $transaction = $result[0];
        return view('customer.transaction', compact('transaction'));
    }

    public function viewReservation(Request $request, $id)
    {
        $reservation = Reservation::where('RESERVE_ID', $id)->where('CUST_ACCNT_ID', auth()->user()->CUST_ACCNT_ID)->with('services')->with('packages')->with('branches')->first();
        return view('customer.reserve_details', compact('reservation'));
    }

    public function reservations()
    {
        return view('customer.reservation');
    }

    public function viewOrderBilling($id)
    {
        $transaction = Transaction::where('ID', $id)->with('products')->get();
        $id = $transaction[0]->ID;
        $total = $transaction[0]->TOTAL;
        $rate = $transaction[0]->SHIPPING_FEE;
        $shipper_address = $transaction[0]->SHIPPER_ADDRESS;
        $biller_address = $transaction[0]->BILLER_ADDRESS;

        $shipper_name = $transaction[0]->SHIPPER_NAME;
        $biller_name = $transaction[0]->BILLER_NAME;
        $date_purchased = $transaction[0]->DATE_PURCHASED;
        $products = [];
        foreach ($transaction[0]->products as $key => $item) {
            $products[] = (object) [
                'name'     => $item->PROD_NAME,
                'price'    => $item->pivot['CURRENT_PRICE'],
                'quantity' => $item->pivot['ORDERED_QUANTITY'],
                'image'    => $item->PROD_IMG
            ];
        }

        return view('mail.invoice', compact('id', 'total', 'rate', 'date_purchased',  'shipper_address', 'products', 'biller_address', 'biller_name', 'shipper_name'));
    }

    public function pdf($id)
    {
        $transaction = Transaction::where('ID', $id)->with('products')->get();
        $id = $transaction[0]->ID;
        $total = $transaction[0]->TOTAL;
        $rate = $transaction[0]->SHIPPING_FEE;
        $shipper_address = $transaction[0]->SHIPPER_ADDRESS;
        $biller_address = $transaction[0]->BILLER_ADDRESS;

        $shipper_name = $transaction[0]->SHIPPER_NAME;
        $biller_name = $transaction[0]->BILLER_NAME;
        $date_purchased = $transaction[0]->DATE_PURCHASED;
        $products = [];
        foreach ($transaction[0]->products as $key => $item) {
            $products[] = (object) [
                'name'     => $item->PROD_NAME,
                'price'    => $item->pivot['CURRENT_PRICE'],
                'quantity' => $item->pivot['ORDERED_QUANTITY'],
                'image'    => $item->PROD_IMG
            ];
        }

        $pdf = PDF::loadView('mail.invoice', [
            'id'              => $id,
            'total'           => $total,
            'rate'            => $rate,
            'date_purchased'  => $date_purchased,
            'shipper_address' => $shipper_address,
            'biller_address'  => $biller_address,
            'biller_name'     => $biller_name,
            'shipper_name'    => $shipper_name,
            'products'        => $products
        ]);


        $fileName = 'Invoice'.date('Y-m-d') . '.pdf';
        return $pdf->download($fileName);
    }
}

